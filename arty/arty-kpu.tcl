#
# Vivado (TM) v2016.4 (64-bit)
#
# arty-kpu.tcl: Tcl script for re-creating project 'arty-kpu'
#
# Generated by Vivado on Tue Apr 04 22:41:27 CEST 2017
# IP Build 1731160 on Wed Dec 14 23:47:21 MST 2016
#
# This file contains the Vivado Tcl commands for re-creating the project to the state*
# when this script was generated. In order to re-create the project, please source this
# file in the Vivado Tcl Shell.
#
# * Note that the runs in the created project will be configured the same way as the
#   original project, however they will not be launched automatically. To regenerate the
#   run results please launch the synthesis/implementation runs as needed.
#
#*****************************************************************************************
# NOTE: In order to use this script for source control purposes, please make sure that the
#       following files are added to the source control system:-
#
# 1. This project restoration tcl script (arty-kpu.tcl) that was generated.
#
# 2. The following source(s) files that were local or imported into the original project.
#    (Please see the '$orig_proj_dir' and '$origin_dir' variable setting below at the start of the script)
#
#    <none>
#
# 3. The following remote source files that were added to the original project:-
#
#    "/home/acorallo/kpu/rtl/wishbone/slaves/bram/dual_port_bram.v"
#    "/home/acorallo/kpu/rtl/includes/uart_defines.v"
#    "/home/acorallo/kpu/rtl/wishbone/slaves/uart/uart_v3.v"
#    "/home/acorallo/kpu/rtl/wishbone/slaves/uart/uart_fifo.v"
#    "/home/acorallo/kpu/rtl/wishbone/slaves/uart/uart_controller.v"
#    "/home/acorallo/kpu/rtl/includes/kpu_conf.v"
#    "/home/acorallo/kpu/rtl/kpu/reg.v"
#    "/home/acorallo/kpu/rtl/kpu/ctrl.v"
#    "/home/acorallo/kpu/rtl/kpu/alu.v"
#    "/home/acorallo/kpu/rtl/wishbone/slaves/uart/wb_uart.v"
#    "/home/acorallo/kpu/rtl/includes/wishbone_defines.v"
#    "/home/acorallo/kpu/rtl/wishbone/master/wb_master.v"
#    "/home/acorallo/kpu/rtl/kpu/mem.v"
#    "/home/acorallo/kpu/rtl/kpu/int_ctrl.v"
#    "/home/acorallo/kpu/arty/arty-kpu.srcs/sources_1/ip/clk_wiz_0_1/clk_wiz_0.xci"
#    "/home/acorallo/kpu/rtl/kpu/kpu.v"
#    "/home/acorallo/kpu/rtl/kpu_soc.v"
#    "/home/acorallo/kpu/arty/constraint/Arty_Master.xdc"
#    "/home/acorallo/kpu/rtl/includes/kpu_conf.v"
#    "/home/acorallo/kpu/rtl/top_kpu_sim.v"
#    "/home/acorallo/kpu/rom/rom.hex"
#    "/home/acorallo/kpu/rtl/includes/uart_defines.v"
#    "/home/acorallo/kpu/rtl/includes/wishbone_defines.v"
#
#*****************************************************************************************

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir "."

# Use origin directory path location variable, if specified in the tcl shell
if { [info exists ::origin_dir_loc] } {
  set origin_dir $::origin_dir_loc
}

variable script_file
set script_file "arty-kpu.tcl"

# Help information for this script
proc help {} {
  variable script_file
  puts "\nDescription:"
  puts "Recreate a Vivado project from this script. The created project will be"
  puts "functionally equivalent to the original project for which this script was"
  puts "generated. The script contains commands for creating a project, filesets,"
  puts "runs, adding/importing sources and setting properties on various objects.\n"
  puts "Syntax:"
  puts "$script_file"
  puts "$script_file -tclargs \[--origin_dir <path>\]"
  puts "$script_file -tclargs \[--help\]\n"
  puts "Usage:"
  puts "Name                   Description"
  puts "-------------------------------------------------------------------------"
  puts "\[--origin_dir <path>\]  Determine source file paths wrt this path. Default"
  puts "                       origin_dir path value is \".\", otherwise, the value"
  puts "                       that was set with the \"-paths_relative_to\" switch"
  puts "                       when this script was generated.\n"
  puts "\[--help\]               Print help information for this script"
  puts "-------------------------------------------------------------------------\n"
  exit 0
}

if { $::argc > 0 } {
  for {set i 0} {$i < [llength $::argc]} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--origin_dir" { incr i; set origin_dir [lindex $::argv $i] }
      "--help"       { help }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
          return 1
        }
      }
    }
  }
}

# Set the directory path for the original project from where this script was exported
set orig_proj_dir "[file normalize "$origin_dir/"]"

# Create project
create_project arty-kpu ./arty-kpu -part xc7a35ticsg324-1L

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Reconstruct message rules
set_msg_config  -ruleid {1}  -id {HDL 9-870}  -string {{CRITICAL WARNING: [HDL 9-870] Macro <DUMP_FILE> is not defined. [/home/acorallo/kpu/rtl/top_kpu_sim.v:63]}}  -suppress  -source 8
set_msg_config  -ruleid {2}  -id {HDL 9-870}  -string {{CRITICAL WARNING: [HDL 9-870] Macro <TEST_PLAN> is not defined. [/home/acorallo/kpu/rtl/top_kpu_sim.v:65]}}  -suppress  -source 8


# Set project properties
set obj [get_projects arty-kpu]
set_property "board_part" "digilentinc.com:arty:part0:1.1" $obj
set_property "default_lib" "xil_defaultlib" $obj
set_property "ip_cache_permissions" "read write" $obj
set_property "ip_output_repo" "/home/acorallo/kpu/arty/arty-kpu.cache/ip" $obj
set_property "sim.ip.auto_export_scripts" "1" $obj
set_property "simulator_language" "Mixed" $obj
set_property "xpm_libraries" "XPM_CDC" $obj
set_property "xsim.array_display_limit" "64" $obj
set_property "xsim.trace_limit" "65536" $obj

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# Set 'sources_1' fileset object
set obj [get_filesets sources_1]
set files [list \
 "[file normalize "$origin_dir/../rtl/wishbone/slaves/bram/dual_port_bram.v"]"\
 "[file normalize "$origin_dir/../rtl/includes/uart_defines.v"]"\
 "[file normalize "$origin_dir/../rtl/wishbone/slaves/uart/uart_v3.v"]"\
 "[file normalize "$origin_dir/../rtl/wishbone/slaves/uart/uart_fifo.v"]"\
 "[file normalize "$origin_dir/../rtl/wishbone/slaves/uart/uart_controller.v"]"\
 "[file normalize "$origin_dir/../rtl/includes/kpu_conf.v"]"\
 "[file normalize "$origin_dir/../rtl/kpu/reg.v"]"\
 "[file normalize "$origin_dir/../rtl/kpu/ctrl.v"]"\
 "[file normalize "$origin_dir/../rtl/kpu/alu.v"]"\
 "[file normalize "$origin_dir/../rtl/wishbone/slaves/uart/wb_uart.v"]"\
 "[file normalize "$origin_dir/../rtl/includes/wishbone_defines.v"]"\
 "[file normalize "$origin_dir/../rtl/wishbone/master/wb_master.v"]"\
 "[file normalize "$origin_dir/../rtl/kpu/mem.v"]"\
 "[file normalize "$origin_dir/../rtl/kpu/int_ctrl.v"]"\
 "[file normalize "$origin_dir/arty-kpu.srcs/sources_1/ip/clk_wiz_0_1/clk_wiz_0.xci"]"\
 "[file normalize "$origin_dir/../rtl/kpu/kpu.v"]"\
 "[file normalize "$origin_dir/../rtl/kpu_soc.v"]"\
]
add_files -norecurse -fileset $obj $files

# Set 'sources_1' fileset file properties for remote files
set file "$origin_dir/../rtl/includes/uart_defines.v"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property "file_type" "Verilog Header" $file_obj

set file "$origin_dir/../rtl/includes/kpu_conf.v"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property "file_type" "Verilog Header" $file_obj

set file "$origin_dir/../rtl/includes/wishbone_defines.v"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sources_1] [list "*$file"]]
set_property "file_type" "Verilog Header" $file_obj


# Set 'sources_1' fileset file properties for local files
# None

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property "include_dirs" "/home/acorallo/kpu/rtl/includes" $obj
set_property "top" "kpu_soc" $obj

# Create 'constrs_1' fileset (if not found)
if {[string equal [get_filesets -quiet constrs_1] ""]} {
  create_fileset -constrset constrs_1
}

# Set 'constrs_1' fileset object
set obj [get_filesets constrs_1]

# Add/Import constrs file and set constrs file properties
set file "[file normalize "$origin_dir/constraint/Arty_Master.xdc"]"
set file_added [add_files -norecurse -fileset $obj $file]
set file "$origin_dir/constraint/Arty_Master.xdc"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets constrs_1] [list "*$file"]]
set_property "file_type" "XDC" $file_obj

# Set 'constrs_1' fileset properties
set obj [get_filesets constrs_1]

# Create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
set files [list \
 "[file normalize "$origin_dir/../rtl/includes/kpu_conf.v"]"\
 "[file normalize "$origin_dir/../rtl/top_kpu_sim.v"]"\
 "[file normalize "$origin_dir/../rom/rom.hex"]"\
 "[file normalize "$origin_dir/../rtl/includes/uart_defines.v"]"\
 "[file normalize "$origin_dir/../rtl/includes/wishbone_defines.v"]"\
]
add_files -norecurse -fileset $obj $files

# Set 'sim_1' fileset file properties for remote files
set file "$origin_dir/../rtl/includes/kpu_conf.v"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Verilog Header" $file_obj

set file "$origin_dir/../rom/rom.hex"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Unknown" $file_obj

set file "$origin_dir/../rtl/includes/uart_defines.v"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Verilog Header" $file_obj

set file "$origin_dir/../rtl/includes/wishbone_defines.v"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "Verilog Header" $file_obj


# Set 'sim_1' fileset file properties for local files
# None

# Set 'sim_1' fileset properties
set obj [get_filesets sim_1]
set_property "top" "top_kpu_sim" $obj
set_property "transport_int_delay" "0" $obj
set_property "transport_path_delay" "0" $obj
set_property "xelab.nosort" "1" $obj
set_property "xelab.unifast" "" $obj

# Create 'synth_1' run (if not found)
if {[string equal [get_runs -quiet synth_1] ""]} {
  create_run -name synth_1 -part xc7a35ticsg324-1L -flow {Vivado Synthesis 2016} -strategy "Vivado Synthesis Defaults" -constrset constrs_1
} else {
  set_property strategy "Vivado Synthesis Defaults" [get_runs synth_1]
  set_property flow "Vivado Synthesis 2016" [get_runs synth_1]
}
set obj [get_runs synth_1]
set_property "steps.synth_design.args.flatten_hierarchy" "none" $obj
set_property -name {steps.synth_design.args.more options} -value {-verilog_define ROM_IMAGE="../../../rom/rom.hex"} -objects $obj

# set the current synth run
current_run -synthesis [get_runs synth_1]

# Create 'impl_1' run (if not found)
if {[string equal [get_runs -quiet impl_1] ""]} {
  create_run -name impl_1 -part xc7a35ticsg324-1L -flow {Vivado Implementation 2016} -strategy "Vivado Implementation Defaults" -constrset constrs_1 -parent_run synth_1
} else {
  set_property strategy "Vivado Implementation Defaults" [get_runs impl_1]
  set_property flow "Vivado Implementation 2016" [get_runs impl_1]
}
set obj [get_runs impl_1]
set_property "steps.write_bitstream.args.bin_file" "1" $obj
set_property "steps.write_bitstream.args.readback_file" "0" $obj
set_property "steps.write_bitstream.args.verbose" "0" $obj

# set the current impl run
current_run -implementation [get_runs impl_1]

puts "INFO: Project created:arty-kpu"
