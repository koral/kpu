# KPU

KPU is a minimal system on chip written used as testbench for the KPU core.

KPU core features:
* RISC load store architecture.
* 32bit word size.
* Fixed 32bit instruction length.
* 3 operands instruction set.
* 32 general purpose registers.
* 5 stage pipeline CPU.
* Non exposed pipeline with short cut control logic to reduce stalls as much as possible.
* Big Endian.
* One instruction per clock (instruction fetch and data access can occur simultaneously).
* Word, half word and byte memory read and write capability.
* External interrupt line.

KPU Soc features:
* On board memory (configurable size).
* Wishbone bus internal connectivity.
* 32 bit general purpose output.
* 32 bit general purpose input.
* Full duplex UART with dedicated FIFO.
* 16 line programmable Interrupt Controller.
* JTAG debug capabilities.

KPU SoC can be either synthesized on FPGA or just executed in simulated environment.

## Resources:

### Source:
<https://gitlab.com/Koral/kpu>
### Documentation:
<https://gitlab.com/koral/kpu/tree/master/doc>
### Toolchain support:
Minimal C compiler

<https://gitlab.com/Koral/kcc>

A more seriuous C compiler targeting kpu
<https://gitlab.com/koral/gcc-kpu>

GNU binutils

<https://gitlab.com/Koral/binutils-gdb-kpu>

## Licensing:
GPL V3.

## Author:
Andrea Corallo <andrea_corallo@yahoo.it>

Copyright (c) 2016-2019 by Andrea Corallo.
