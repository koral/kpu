;;; Echo back every byte received on uart

#pragma offset 0xffff8000

int0:	JMP code_start	; 0x0
	NOP		; 0x4
int1:	JMP read_uart	; 0x8
	LDAW R4, 0xfffffffc	; 0xC We load on R4 the int number that caused the raise
int2:	MOVI R1, 0x666	; 0x10 This has not to be executed
	NOP		; 0x14
int3:	NOP		; 0x18
	NOP		; 0x20

code_start:

reset_uart_int: ;; Enable UART interrupt into the control register
	MOVI	R1, (1 << 3)
	STAW	R1, 0xffffffec
reset_ic:
	STAW R0, 0xfffffffc	; We reset the interrupt controller
	ORI SR, SR, (0x1 << 4)	; We enable the external interrupt

wait:
	JMP wait
	NOP

read_uart:
	;; Read received byte into R2
	LDAW	R2, 0xffffffd0
	;; Send it back!
	STAW	R2, 0xffffffd8
	JMP	reset_ic
	NOP
