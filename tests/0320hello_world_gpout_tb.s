;; Use the last byte of the GPOUT to print a classical 'Hello Word!!'
;; for a couple of times
#pragma offset 0xffff8000

start:		MOVI	R8, data	; R8 is the pointer to the data
		MOVI	R9, 14		; R9 keep the string len
load_char:	LDB	R4, R8, 0
		STAW	R4, 0xfffffff8

		MOVI	R2, 10		; waste time for 10 loops
wait1:		SUBI	R2, R2, 1
		CMPI	R2, 0x0
		BNEQ	wait1
		NOP

		ADDI	R8, R8, 1
		SUBI	R9, R9, 1
		CMPI	R9, 0
		BNEQ	load_char	; load the next character
		NOP

		JMP	start		; restart from the beginning
		NOP

data:
.ascii		"Hello World!! "
