#pragma offset 0xffff8000
	MOVI    R0, -1	     ; 0x0
	MOVI    R1, 1        ; 0x4
label1: CMP     R1, R0       ; 0x8
        BG      label1       ; 0xc
label2: CMPU    R1, R0       ; 0x10
        BG      label2       ; 0x14
        NOP
