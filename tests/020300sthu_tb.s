data_rw:
	;; We use this lable just to point to ram

#pragma offset 0xffff8000

        MOVI	R0, data_ro
	MOVI	R5, data_rw
        MOVI	R1, 0xAABB
	MOVI	R2, 0xCCDD
 	; This will now work cause we are in rom
	STHU	R1, R0, 0
	STHU	R2, R0, 2
	LDHU	R3, R0, 0
	LDHU	R4, R0, 2
	STHU	R1, R5, 0
	STHU	R2, R5, 2
	LDHU	R3, R5, 0
	LDHU	R4, R5, 2

data_ro:
.word	0x0
