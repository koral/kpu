////////////////////////
// Test the registers //
////////////////////////
`include "reg.v"
`include "kpu_conf.v"

module test_reg;
	reg clk;
	reg rst;
	reg [`N-1:0] in0;
	reg [`N-1:0] in1;
	wire [`N-1:0] out0;
	wire [`N-1:0] out1;


	initial begin
		clk <= 0;
		rst <= 0;

		$dumpfile("reg.vcd");
		$dumpvars;

		in0 <= `N'h0;
		in1 <= `N'h0;
		$display("Starting no reset register test...");
		$display("clk\tin\t\tout");
		$monitor("%x\t%x\t%x", clk, in0, out0);
		#50;

		in1 <= `N'h0;
		$display("Starting sync reset register test...");
		$display("clk\treset\t\tin\t\tout");
		$monitor("%x\t%x\t\t%x\t%x", clk, rst, in1, out1);
		#20 rst <= 1;
		#10 rst <= 0;
		#20 $finish;
	end

	always #5 clk <= !clk;

	always @(posedge clk)
		#1 begin
			in0  = out0 + 1;
			in1  = out1 + 1;
		end

	// no rst reg
	nr_reg reg0(.clk(clk),
		    .in(in0),
		    .out(out0));
	// sync rst reg
	sr_reg reg1(.clk(clk),
		    .rst(rst),
		    .in(in1),
		    .out(out1));

endmodule
