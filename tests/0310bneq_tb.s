#pragma offset 0xffff8000
	MOVI	R2, 4		; 0x0	R2 <= 0x4
label:	SUBI	R2, R2, 1	; 0x4	R2 <= R2 - 1
	CMPI   	R2, 0x0		; 0x8
	BNEQ	label		; 0xA	if (R2 != 0): JMP 0x4
	NOP
