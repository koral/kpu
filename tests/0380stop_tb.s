#pragma offset 0xffff8000
	;; Loop while stop is asserted during execution
	MOVI R3, 0x0
	MOVI R1, 0x1
loop_start:
	ADD R3, R3, R1
	JMP loop_start
	NOP
