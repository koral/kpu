;; Stop KPU and read R1 using jtag

#pragma offset 0xffff8000

	MOVI	R1, 0
loop:	ADDI	R1, R1, 1
	JMP	loop
	NOP
