#pragma offset 0xffff8000
	MOVI	R2, 7000	; waste time for 7000 iterations before reading
wait:	SUBI	R2, R2, 1
	CMPI	R2, 0x0
	BNEQ	wait
	NOP

	;; Then read it
	LDAW	R1, 0xffffffd0
	LDAW	R1, 0xffffffd0
	LDAW	R1, 0xffffffd0
	LDAW	R1, 0xffffffd0
	LDAW	R1, 0xffffffd0
	LDAW	R1, 0xffffffd0
