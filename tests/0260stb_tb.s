ram:
#pragma offset 0xffff8000

int0:	JMP test
	NOP
int1:	NOP
	NOP
int2:	NOP
	NOP
int3:	NOP
	NOP
int4:	NOP
	NOP
int5:	SUBI R29, R29, 0xc
        JMPR R29
	NOP
	NOP
	NOP
	NOP
test:   MOVI	R0, ram
	LDAW	R1, data0
	LDAW	R2, data1
	STB	R1, R0, 0
        LDB	R3, R0, 0
	STB	R2, R0, 1       ; This should generate an interruption
	MOVI	R8,0x1
	MOVI	R8,0x2
	MOVI	R8,0x3
	MOVI	R8,0x4
	MOVI	R8,0x5
	MOVI	R8,0x6
	MOVI	R8,0x7
	MOVI	R8,0x8
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
data0:
.word	0x00000001
data1:
.word	0x10000001
