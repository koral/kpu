#pragma offset 0xffff8000
	;; LD + RRI ALU inst
	LDAW	R1, data1	; R1 <= 0x20
	NOP
	ADDI	R0, R1, 0x40	; R0 <= 0x60
	LDW	R1, R2, data2	; R1 <= 0x80
	NOP
	ADDI	R8, R1, 0x40	; R8 <= 0xc0
	;; LD + RRR ALU inst
	LDAW	R1, data1 	; R1 <= 0x20
	NOP
	ADD	R0, R1, R3	; R0 <= 0x20
	LDW	R1, R2, data2	; R1 <= 0x80
	NOP
	ADD	R8, R1, R3	; R8 <= 0x80
data1:
.word	0x20
data2:
.word	0x80
