#pragma offset 0xffff8000
int0:	JMP code	; 0x0
	NOP		; 0x4
int1:	JMPR R29	; 0x8 PC <= 0x24 Resume execution after interrupt
	NOP		; 0xC
int2:	MOVI R1, 0x666	; 0x10 This has not to be executed
	NOP		; 0x14
int3:	NOP		; 0x18
	NOP		; 0x20
code:	INT 1		; 0x22 PC <= 0x8 R29 <= 0x24
	MOVI R3, 0x123	; 0x24 R3 <= 0x123 This will be executed after the
			; interrupt handling
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
