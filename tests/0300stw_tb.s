data_rw:

#pragma offset 0xffff8000

        MOVI	R1, data_ro	; 0x0	R1 <= 0x10
        MOVI	R3, data_rw	; 0x0	R1 <= 0x10
	MOVI	R2, 0x1983	; 0x4	R2 <= 0x1983
	STW	R2, R1, 0	; 0x8
        LDW	R0, R1, 0	; 0xC	R0 <= 0x666
	STW	R2, R3, 0	; 0x8	*data <= 0x1983
        LDW	R0, R3, 0	; 0xC	R0 <= 1983

data_ro:
.word	0x666			; 0x10
