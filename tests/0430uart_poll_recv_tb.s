#pragma offset 0xffff8000

start:	NOP
				;;;  Poll the uart till we have something to read
wait:	LDAW	R2, 0xffffffe8
	CMPI	R2, 0x18
	BNEQ	wait
	NOP
				;;;  Then read it in R1
	LDAW	R1, 0xffffffd0
	JMP	start
