#pragma offset 0xffff8000

reset_uart:	MOVI	R6, 0x0
		STAW 	R6, 0xffffffec	; We reset the uart


		MOVI R1, 0x000000aa
		MOVI R3, 0x000000bb
		LDAW R2, uart_addr
		STW R1, R2, 0
		STW R3, R2, 0
loop:		NOP
		JMP loop
		NOP
uart_addr:
.word		0xffffffd8
