ram:

#pragma offset 0xffff8000

int0:	JMP test
	NOP
int1:	NOP
	NOP
int2:	NOP
	NOP
int3:	NOP
	NOP
int4:	NOP
	NOP
int5:	SUBI R29, R29, 0xc
        JMPR R29
	NOP
	NOP
	NOP
	NOP
test:	MOVI	R9, ram
	LDAW	R1, data0	; R1 <= 0xC0000001
	LDAW	R2, data1	; R2 <= 0x00000001
	STH	R1, R9, 0
	LDH	R3, R9, 0	; R3 <= 0x00000001 = 0xC0000001 & 0xffff
	STH	R2, R9, 2	; This should generate an interruption
	MOVI	R8,0x1
	MOVI	R8,0x2
	MOVI	R8,0x3
	MOVI	R8,0x4
	MOVI	R8,0x5
	MOVI	R8,0x6
	MOVI	R8,0x7
	MOVI	R8,0x8
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
	NOP
data0:
.word	0x00000001
data1:
.word	0xC0000001
