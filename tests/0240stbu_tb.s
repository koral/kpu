data_rw:
	;; We use this lable to point to ram

#pragma offset 0xffff8000

        MOVI	R0, data_rw
        MOVI	R1, 0xAA
	MOVI	R2, 0xBB
        MOVI	R3, 0xCC
	MOVI	R4, 0xDD
	STBU	R1, R0, 0
	STBU	R2, R0, 1
	STBU	R3, R0, 2
	STBU	R4, R0, 3
	LDAW	R5, data_rw
