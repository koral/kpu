;; Send a classical 'Hello Word!!' on UART

#pragma offset 0xffff8000

reset_uart:	MOVI	R6, 0x0
		STAW 	R6, 0xffffffec	; We reset the uart

start:		MOVI	R8, data	; R8 is the pointer to the data
		LDAW	R10, uart_write	; R10 is the pointer to the UART out reg
		MOVI	R9, 14		; R9 keep the string len
load_char:	LDB	R4, R8, 0
		STW	R4, R10, 0

		ADDI	R8, R8, 1
		SUBI	R9, R9, 1
		CMPI	R9, 0
		BNEQ	load_char	; load the next character
		NOP

		MOVI	R2, 1000	; waste time for 1000 loops before restarting
wait1:		SUBI	R2, R2, 1
		CMPI	R2, 0x0
		BNEQ	wait1
		NOP

		JMP	start		; restart from the beginning
		NOP

data:
.ascii		"Hello World!! "
uart_write:
.word		0xffffffd8
