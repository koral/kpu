;;; Set the sp, fp + return pointer and jump into main
_start:
	MOVI	SP, (0x1000 - 4)
	MOV	FP, SP
	JMP	main
	MOVI	R28, 0xffffffff

