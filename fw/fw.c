/*
  send a classic hello world on uart
*/

unsigned int *__uart_reset = 0xffffffec;
unsigned int *__uart_out = 0xffffffd8;

char *str = "Hello world!";

void uart_reset(void)
{
	*__uart_reset = 0x0;
}

void print_char(char c)
{
	*__uart_out = c;
}

int main(void)
{
	char *p = str;

	uart_reset();

	while (*p) {
		print_char(*p);
		p++;
	}

	return 0;
}
