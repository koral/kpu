#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int ch, to_pad, line = 0;

	if (argc != 2)
		goto exit_error;

	to_pad = atoi(argv[1]);

	while ((ch = getchar()) != EOF) {
		if (ch == ('\n'))
			++line;
		putchar(ch);
	}

	if (line >= to_pad)
		return 0;

	to_pad -= line;
	while (to_pad--)
		printf("0x0\n");

	return 0;
exit_error:

	printf("Usage:\n\t./padhex nlines < in.hex > out.hex");
	return 1;
}
