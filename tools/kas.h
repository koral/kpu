#ifndef _kas_h
#define _kas_h

#define MAX_LABELS	(1 << 8)  /* Max number of defined labels */
#define MAX_LABEL_LEN	(1 << 8)  /* Max number of characters for a label */
#define MAX_LABEL_REF   (1 << 10) /* Max references to labels */

extern unsigned line;
int yyerror(const char* format, ...);
void free_labels();

#endif
