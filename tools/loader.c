/***********************************************************************************************/
/* Original tty code from:                                                                     */
/* http://stackoverflow.com/questions/6947413/how-to-open-read-and-write-from-serial-port-in-c */
/*                                                                                             */
/* Wait from the initial ROM message "KPU ROM waiting for firmware..." and upload a firmware.  */
/*                                                                                             */
/* Usage: ./loader /dev/ttyS0 < ../fw/fw.bin                                                   */
/***********************************************************************************************/


#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>

#define FW_SIZE (1024 << 2) /* 4KB */

int set_interface_attribs(int fd, int speed, int parity)
{
	struct termios tty;

	memset(&tty, 0, sizeof tty);
	if (tcgetattr(fd, &tty) != 0) {
		fprintf(stderr, "error %d from tcgetattr", errno);
		return -1;
	}

	cfsetospeed(&tty, speed);
	cfsetispeed(&tty, speed);

	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     /* 8-bit chars */
	/* disable IGNBRK for mismatched speed tests; otherwise receive break */
	/* as \000 chars */
	tty.c_iflag &= ~IGNBRK;                 /* disable break processing */
	tty.c_lflag = 0;                        /* no signaling chars, no echo, */
	                                        /* no canonical processing */
	tty.c_oflag = 0;                        /* no remapping, no delays */
	tty.c_cc[VMIN] = 0;                     /* read doesn't block */
	tty.c_cc[VTIME] = 120;                  /* 120sec read timeout */

	tty.c_iflag &= ~(IXON | IXOFF | IXANY); /* shut off xon/xoff ctrl */

	tty.c_cflag |= (CLOCAL | CREAD);        /* ignore modem controls, */
	                                        /* enable reading */
	tty.c_cflag &= ~(PARENB | PARODD);      /* shut off parity */
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;

	if (tcsetattr(fd, TCSANOW, &tty) != 0) {
		fprintf(stderr, "error %d from tcsetattr", errno);
		return -1;
	}

	return 0;
}

void set_blocking(int fd, int should_block)
{
	struct termios tty;

	memset(&tty, 0, sizeof tty);
	if (tcgetattr(fd, &tty) != 0) {
		fprintf(stderr, "error %d from tggetattr", errno);
		return;
	}

	tty.c_cc[VMIN] = should_block ? 1 : 0;
	tty.c_cc[VTIME] = 5;                    /* 5 seconds read timeout */

	if (tcsetattr(fd, TCSANOW, &tty) != 0)
		fprintf(stderr, "error %d setting term attributes", errno);
}


int main(int argc, char *argv[])
{
	int n, tty, i;
	FILE *fw;
	char *portname = "/dev/ttyUSB0";
	char *rom_msg = "KPU ROM waiting for firmware...";
	char ch;
	char buf [100];

	if (argc > 1)
		portname = argv[1];

	tty = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
	fw = stdin;

	if (tty < 0) {
		fprintf(stderr, " error %d opening %s: %s", errno, portname,
			strerror(errno));
		return -1;
	}

	if (!fw) {
		fprintf(stderr, "Could not find firmware image\n");
		return -1;
	}

	set_interface_attribs(tty, B115200, 0); /* set speed to 115,200 bps,
	                                         * 8n1(no parity) */
	n = read(tty, buf, 31);                 /* read up the 31 characters of
	                                         * the intial ROM message */
	buf[32] = '0';
	printf("ROM msg: %s", buf);
	if (strcmp(buf, rom_msg)) {
		fprintf(stderr, "ROM init message mismatch\n");
		return -1;
	}

	printf("\nUploading firmware...\n");
	for (i = 0; i < FW_SIZE; i++) {
		n = fread(buf, 1, 1, fw);
		if (n != 1)
			buf[0] = 0;
		write(tty, buf, 1);
		read(tty, &ch, 1);
		if (ch != buf[0]) {
			fprintf(stderr, "Data mismatch\n");
			return -1;
		}
		if (!(i % 32)) {
			printf("Sent %d%%\r", 100 * i / FW_SIZE);
			fflush(stdout);
		}
	}
	printf("Sent 100%%\nROM msg: ");

	while (1) {
		n = read(tty, buf, 1);
		putchar(buf[0]);
		fflush(stdout);
	}

	return 0;
}
