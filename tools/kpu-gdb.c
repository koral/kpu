//-----------------------------------------------------------------------------
// Title	 : KPU GDB bridge
// Project	 : KPU
//-----------------------------------------------------------------------------
// File		 : kpu-gdb.c
// Author	 : acorallo  <andrea_corallo@yahoo.it>
// Created	 : 13.06.2017
//-----------------------------------------------------------------------------
// Description :
// Open a socket to handle gdb commands
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 13.06.2017: created
//-----------------------------------------------------------------------------

#define _GNU_SOURCE

#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <arpa/inet.h>

#define VERBOSE                 1

#define LOG_JPLAN               0       /* Generate a jplan file suitable
					   for simulation  */

#define SIM_PORT                4567
#define RSP_SRV_PORT            1983
#define MAX_DBG_PACKET          2048

#define IR_BITS                 4

#define EXTEST                  0x0
#define SAMPLE_PRELOAD          0x1
#define IDCODE                  0x2
#define DBG_CONF                0x8
#define DBG_IR                  0xc
#define DBG_DATA_SEND           0x6
#define DBG_DATA_RECV           0x3
#define BYPASS                  0xf

#define DBG_CONF_DR_BITS        8
#define DBG_CONF_TCK_USED               (1 << 0)
#define DBG_CONF_SEND_INST      (1 << 1)
#define DBG_CONF_SEND_DATA      (1 << 2)
#define DBG_CONF_RECV_DATA      (1 << 3)
#define DBG_CONF_INC_PC         (1 << 4)

#define TCK                     (1 << 0)
#define TRST                    (1 << 1)
#define TDI                     (1 << 2)
#define TMS                     (1 << 3)

#define WRITE_REG_CYCLES        10      /* Number of cpu cycles used by dbg_write_reg() */

#define NOP_BITSEQ              0x34000000

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)			\
	(byte & 0x80 ? '1' : '0'),		\
	(byte & 0x40 ? '1' : '0'),	\
	(byte & 0x20 ? '1' : '0'),	\
	(byte & 0x10 ? '1' : '0'),	\
	(byte & 0x08 ? '1' : '0'),	\
	(byte & 0x04 ? '1' : '0'),	\
	(byte & 0x02 ? '1' : '0'),	\
	(byte & 0x01 ? '1' : '0')

#define BYTE_TO_J_PATTERN "%c%c%c%c"
#define BYTE_TO_J(byte)			\
	(byte & 0x08 ? '1' : '0'),	\
	(byte & 0x04 ? '1' : '0'),	\
	(byte & 0x02 ? '1' : '0'),	\
	(byte & 0x01 ? '1' : '0')


int fd_sock;

struct {
	/* gdb stub server info */
	int		sock_fd;
	int		client_fd;
	FILE *		client_fp;
	char		client_ip[INET_ADDRSTRLEN];
	uint16_t	client_port;

	int		interrupted;
} rsp;

/* Jtag states defined are just the one we rest on */
enum j_state_t { test_logic_reset = 0,
		 run_test_idle = 1,
		 select_dr_scan = 2,
		 shift_dr = 3,
		 exit1_dr = 4,
		 shift_ir = 5,
		 exit1_ir = 6,
		 invalid_state = 7 } j_state;

/*********************/
/* Utility functions */
/*********************/

char int_to_hex(unsigned val)
{
//TODO just use array "0123456789abcdef"
// see http://codereview.stackexchange.com/questions/30579/how-can-this-integer-to-hex-string-code-be-improved
	if (val >= 16)
		return -1;
	if (val < 10)
		return val + '0';
	else
		return val - 10 + 'a';
}


int hex_to_int(char ch)
{
	if ('0' <= ch && ch <= '9')
		return ch - '0';
	else if ('a' <= ch && ch <= 'f')
		return ch - 'a' + 10;
	else if ('A' <= ch && ch <= 'F')
		return ch - 'A' + 10;
	else
		return 0;
}

/**********************/
/* Jplan log creation */
/**********************/

static FILE *fp_jplan = NULL;

void jplan_open(void)
{
	fp_jplan = fopen("gdb.jplan", "w+");
}

void log_byte_jplan(char c)
{
	if (!fp_jplan)
		jplan_open();

	fprintf(fp_jplan, BYTE_TO_J_PATTERN "\n", BYTE_TO_J(c));
}

void log_str_jplan(const char *format, va_list ap)
{
	if (!fp_jplan)
		jplan_open();

	fprintf(fp_jplan,"# ");
	vfprintf(fp_jplan, format, ap);
}

/************************/
/* printf like wrappers */
/************************/

void kprintf(const char *format, ...)
{
	va_list args;

	va_start(args, format);
	vprintf(format, args);
	va_end(args);

	if (LOG_JPLAN) {
		va_start(args, format);
		log_str_jplan(format, args);
		va_end(args);
	}
}

void kfprintf(FILE *stream, const char *format, ...)
{
	va_list args;

	va_start(args, format);
	vfprintf(stream, format, args);
	va_end(args);

	if (LOG_JPLAN) {
		va_start(args, format);
		log_str_jplan(format, args);
		va_end(args);
	}
}

/****************************/
/* Phisical layer functions */
/****************************/

int sock_init_conn(char *addr)
{
	struct sockaddr_in serv_addr;

	if ((fd_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		kfprintf(stderr, "Error: Could not create socket.\n");
		return -1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(SIM_PORT);

	if (inet_pton(AF_INET, addr, &serv_addr.sin_addr) <= 0) {
		kfprintf(stderr, "Error: inet_pton error occured.\n");
		return -1;
	}

	if (connect(fd_sock, (struct sockaddr *)&serv_addr,
		    sizeof(serv_addr)) < 0) {
		kfprintf(stderr, "Error: Socket connection failed.\n");
		return -1;
	}

	return fd_sock;
}

int sock_recv_byte(char *c)
{
	if (!read(fd_sock, c, 1))
		return -1;

	return 0;
}

int sock_send_byte(char c)
{
	if (VERBOSE > 1)
		kprintf("Debug: %s() sending byte: "BYTE_TO_BINARY_PATTERN "\n",
		       __FUNCTION__, BYTE_TO_BINARY(c));
	usleep(1); /* FIXME: Why this awfullness works? */
	if (!write(fd_sock, &c, 1))
		return -1;

	return 0;
}

/*****************************************/
/* Physical abstracted IO function layer */
/*****************************************/

int init_conn(char *target)
{
	int res;

	res = sock_init_conn(target);
	if (res == -1)
		kfprintf(stderr, "Error: Connection failed.\n");

	return res;
}

int recv_byte(char *c)
{
	int res;

	res = sock_recv_byte(c);
	if (res == -1)
		kfprintf(stderr, "Error: Receiving byte failed.\n");

	return res;
}

int send_byte(char c)
{
	int res;

        if (LOG_JPLAN)
		log_byte_jplan(c);

	res = sock_send_byte(c);
	if (res == -1)
		kfprintf(stderr, "Error: Sending byte failed.\n");

	return res;
}

/************************/
/* Jtag layer functions */
/************************/

int j_get_tdo(char *c)
{
	/* 0x80 is the magic key to get back the tdo */
	if (send_byte(0x80) == -1)
		return -1;
	if (recv_byte(c) == -1)
		return -1;

	return 0;
}

int j_clock(char c)
{
	char rcvd;

	c |= TRST;

	if (send_byte(c) == -1)
		return -1;
	if (recv_byte(&rcvd) == -1)
		return -1;
	rcvd &= 0xf;
	if (rcvd != c)
		kfprintf(stderr, "Warning: Incoherent data received. "
			"0x%x != 0x%x\n", rcvd, c);

	c |= TCK;

	if (send_byte(c) == -1)
		return -1;
	if (recv_byte(&rcvd) == -1)
		return -1;
	rcvd &= 0xf;
	if (rcvd != c)
		kfprintf(stderr, "Warning: Incoherent data received. "
			"0x%x != 0x%x\n", rcvd, c);

	return 0;
}

/* Wherever you are move to test-logic-reset state */
int j_reset(void)
{
	int i;

	for (i = 0; i < 5; i++)
		if (j_clock(TMS) == -1) {
			kfprintf(stderr, "Error: Could not move to "
				"test-logic-reset state.\n");
			return -1;
		}

	return 0;
}

int j_move_to(enum j_state_t s)
{
	int res = 0;

try:
	if (VERBOSE)
		kprintf("Debug: %s() moving state from %d to %d.\n",
		       __FUNCTION__, j_state, s);

	switch (s) {
	case test_logic_reset:
		res |= j_reset();
		break;
	case run_test_idle:
		if (j_state == test_logic_reset) {
			res |= j_clock(0);
		} else if (j_state == exit1_dr ||
			   j_state == exit1_ir) {
			res |= j_clock(TMS);
			res |= j_clock(0);
		} else {
			goto panic;
		}
		break;
	case select_dr_scan:
		if (j_state == run_test_idle) {
			res |= j_clock(TMS);
		} else if (j_state == exit1_dr ||
			   j_state == exit1_ir) {
			res |= j_clock(TMS);
			res |= j_clock(TMS);
		} else {
			res |= j_move_to(run_test_idle);
			goto try;
		}
		break;
	case shift_dr:
		if (j_state == select_dr_scan) {
			res |= j_clock(0);
			res |= j_clock(0);
		} else {
			res |= j_move_to(select_dr_scan);
			goto try;
		}
		break;
	case shift_ir:
		if (j_state == select_dr_scan) {
			res |= j_clock(TMS);
			res |= j_clock(0);
			res |= j_clock(0);
		} else {
			res |= j_move_to(select_dr_scan);
			goto try;
		}
		break;
	default:
		goto panic;
	}

	if (res)
		j_state = invalid_state;
	else
		j_state = s;

	return res;
panic:
	kfprintf(stderr, "Error: Don't know how to go from %d to %d.\n",
		j_state, s);

	return -1;
}

/* This function assume to be already in shift-IR or shift-DR state */
int j_shift_bits(unsigned in, unsigned *out, int num_bits)
{
	char c;
	int res = 0;

	if (VERBOSE)
		kprintf("Debug: %s() sending %d of 0x%x.\n",
		       __FUNCTION__, num_bits, in);

	if (!num_bits)
		return -1;
	if (j_state != shift_dr && j_state != shift_ir)
		return -1;
	if (out)
		*out = 0;

	while (--num_bits) {
		if (in & 1)
			res |= j_clock(TDI);
		else
			res |= j_clock(0);
		in >>= 1;
		if (out) {
			res |= j_get_tdo(&c);
			*out = (c << 31) | ((*out) >> 1);
		}
	}

	/* Send the last bit changing state */
	if (in & 1)
		res |= j_clock(TDI | TMS);
	else
		res |= j_clock(TMS);

	if (out) {
		res |= j_get_tdo(&c);
		*out = (c << 31) | ((*out) >> 1);
	}


	if (res)
		return -1;

	if (j_state == shift_dr)
		j_state = exit1_dr;
	if (j_state == shift_ir)
		j_state = exit1_ir;

	return 0;
}

int j_run_test(int i)
{
	int res = 0;

	if (!i)
		return -1;

	if (VERBOSE)
		kprintf("Debug: %s() running %d times.\n",
		       __FUNCTION__, i);

	if (j_state != run_test_idle)
		res |= j_move_to(run_test_idle);

	while (--i && !res)
		res |= j_clock(0);

	if (res)
		return -1;

	return 0;
}

int j_shift_ir(unsigned ir)
{
	if (j_move_to(shift_ir) == -1)
		return -1;

	if (j_shift_bits(ir, NULL, IR_BITS) == -1)
		return -1;

	return 0;
}

int j_shift_dr(unsigned in, unsigned *out, unsigned nbits)
{
	if (j_move_to(shift_dr) == -1)
		return -1;

	if (j_shift_bits(in, out, nbits) == -1)
		return -1;

	return 0;
}


/***********************/
/* top layer functions */
/***********************/

/* By default we lay in Select DR-Scan state */
int dbg_init(char *target)
{
	if (init_conn(target) == -1)
		return -1;

	if (j_move_to(test_logic_reset) == -1)
		return -1;

	return 0;
}

int dbg_single_step(unsigned steps)
{
	if (j_shift_ir(DBG_CONF) == -1)
		return -1;

	if (j_shift_dr(DBG_CONF_TCK_USED | DBG_CONF_INC_PC, NULL,
		       DBG_CONF_DR_BITS) == -1)
		return -1;

	if (j_run_test(steps) == -1)
		return -1;

	return 0;
}

int dbg_read_reg(int reg, unsigned *data)
{
	int inst;

	if (reg > 31)
		goto exit_error;

	if (j_shift_ir(DBG_CONF) == -1)
		goto exit_error;

	if (j_shift_dr(DBG_CONF_TCK_USED |
		       DBG_CONF_SEND_INST |
		       DBG_CONF_RECV_DATA,
		       NULL,
		       DBG_CONF_DR_BITS) == -1)
		goto exit_error;

	if (j_shift_ir(DBG_IR) == -1)
		goto exit_error;

	/* STAW Rx, 0x0 */
	inst = 0x30000000 | (reg << 21);

	if (j_shift_dr(inst, NULL, 32) == -1)
		goto exit_error;

	/* Commit to the pipe */
	if (j_run_test(1) == -1)
		goto exit_error;

	if (j_shift_ir(DBG_IR) == -1)
		goto exit_error;

	/* Nexts are gonna be a NOPs */
	if (j_shift_dr(NOP_BITSEQ, NULL, 32) == -1)
		goto exit_error;

	/* Advance the pipe */
	if (j_run_test(2) == -1)
		goto exit_error;

	/* Read data back */
	if (j_shift_ir(DBG_DATA_RECV) == -1)
		goto exit_error;

	if (j_shift_dr(0, data, 32) == -1)
		goto exit_error;

	if (VERBOSE)
		kprintf("Debug: %s() has read reg %d with value 0x%x\n",
		       __FUNCTION__, reg, *data);

	return 0;

exit_error:
	kfprintf(stderr, "Error: %s() is exiting with error.\n", __FUNCTION__);


	return -1;
}

int dbg_write_reg(int reg, int data)
{
	int inst;

	if (reg > 31)
		goto exit_error;

	if (j_shift_ir(DBG_CONF) == -1)
		goto exit_error;

	if (j_shift_dr(DBG_CONF_TCK_USED |
		       DBG_CONF_SEND_INST |
		       DBG_CONF_SEND_DATA,
		       NULL,
		       DBG_CONF_DR_BITS) == -1)
		goto exit_error;

	if (j_shift_ir(DBG_IR) == -1)
		goto exit_error;

	/* "LDAW R0, 0x0" = 0x2c000000 */
	inst = 0x2c000000 | (reg << 21);

	if (j_shift_dr(inst, NULL, 32) == -1)
		goto exit_error;

	if (j_shift_ir(DBG_DATA_SEND) == -1)
		goto exit_error;

	if (j_shift_dr(data, NULL, 32) == -1)
		goto exit_error;

	/* Commit to the pipe */
	if (j_run_test(1) == -1)
		goto exit_error;

	if (j_shift_ir(DBG_IR) == -1)
		goto exit_error;

	/* Nexts are gonna be a NOPs */
	if (j_shift_dr(NOP_BITSEQ, NULL, 32) == -1)
		goto exit_error;

	/* Advance and flush pipe */
	if (j_run_test(4) == -1)
		goto exit_error;

	if (j_move_to(select_dr_scan) == -1)
		goto exit_error;

	return 0;

exit_error:
	kfprintf(stderr, "Error: %s() is exiting with error.\n", __FUNCTION__);


	return -1;
}

int dbg_read_word(unsigned addr, unsigned *data)
{
	unsigned inst, r16;

	if (addr & 0x3)
		/* read must be word aligned */
		goto exit_error;

	if (dbg_read_reg(0, &r16) == -1)
		goto exit_error;

	if (j_shift_ir(DBG_CONF) == -1)
		goto exit_error;

	if (j_shift_dr(DBG_CONF_SEND_INST |
		       DBG_CONF_RECV_DATA,
		       NULL,
		       DBG_CONF_DR_BITS) == -1)
		goto exit_error;

	if (j_shift_ir(DBG_IR) == -1)
		goto exit_error;

	/* LDAW R16, addr */
	inst = 0x2c000000 | (16 << 21) | ((addr >> 2) & ((1 << 21) - 1));

	if (j_shift_dr(inst, NULL, 32) == -1)
		goto exit_error;

	/* This is just to waste some time in run_test_idle */
	if (j_run_test(1) == -1)
		goto exit_error;

	/* FIXME: why does this fix the thing? */
	if (dbg_read_reg(31, data) == -1)
		goto exit_error;

	if (dbg_read_reg(16, data) == -1)
		goto exit_error;

	if (dbg_write_reg(0, r16) == -1)
		goto exit_error;

	return 0;

exit_error:
	kfprintf(stderr, "Error: %s() is exiting with error.\n", __FUNCTION__);

	return -1;
}

/****************************/
/* Gdb RSP handler function */
/****************************/

int get_connection()
{
	int rc;
	int reuse = 1;
	struct sockaddr_in sockaddr;

	int status = -1;
	int sock_fd = socket(AF_INET, SOCK_STREAM, 0);

	if (sock_fd == -1)
		goto err;

	sockaddr.sin_family = AF_INET;
	/* 127.0.0.1 == 0x7F000001 */
	sockaddr.sin_addr.s_addr = htonl(0x7f000001); /*  */

	/* RSP_SRV_PORT udp and tcp are the official registered remote debug ports */
	sockaddr.sin_port = htons(RSP_SRV_PORT);

	rc = setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR,
			(void *)&reuse, sizeof(reuse));

	if (rc != 0)
		goto err;

	rc = bind(sock_fd, (struct sockaddr *)&sockaddr, sizeof(sockaddr));

	if (rc != 0)
		goto err;

	rc = listen(sock_fd, 1);

	if (rc != 0)
		goto err;

	rsp.sock_fd = sock_fd;

	return 0;

err:
	return status;
}


int rsp_srv_init()
{
	int rc, clientfd, len;
	struct sockaddr_in client_addr = { 0 };
	socklen_t addr_size = sizeof(struct sockaddr);
	struct timeval timeout;

	memset(&client_addr, 0, sizeof(struct sockaddr_in));
	get_connection();
	clientfd = accept(rsp.sock_fd, (struct sockaddr *)&client_addr, &addr_size);

	if (clientfd == -1)
		goto exit_error;

	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	rc = setsockopt(clientfd, SOL_SOCKET, SO_RCVTIMEO,
			(char *)&timeout, sizeof(timeout));
	if (rc != 0)
		goto exit_error;

	rc = setsockopt(clientfd, SOL_SOCKET, SO_SNDTIMEO,
			(char *)&timeout, sizeof(timeout));
	if (rc != 0)
		goto exit_error;

	/* preserve file descriptor */
	rsp.client_fd = clientfd;

	/* record remote IP */
	len = sizeof(client_addr);
	rc = getpeername(clientfd, (struct sockaddr *)&client_addr,
			 (socklen_t *)&len);
	if (rc != 0)
		goto exit_error;

	rsp.client_port = ntohs(client_addr.sin_port);
	inet_ntop(AF_INET, &client_addr.sin_addr, rsp.client_ip, INET_ADDRSTRLEN);

	kprintf("New connection from %s:%d\n", rsp.client_ip,
	       rsp.client_port);

	/* preserve FILE for stream ops */
	rsp.client_fp = fdopen(clientfd, "w+");
	if (!rsp.client_fp)
		goto exit_error;

	return 0;

exit_error:
	return -1;
}

void write_hex_byte(char *dest, uint8_t byte)
{
	dest[0] = int_to_hex(byte >> 4);
	dest[1] = int_to_hex(byte & 0xf);
}

char log_getc(FILE *fp)
{
	int ch = getc(fp);

	/* EAGAIN here because the setsockopt	*/
	if (errno == EWOULDBLOCK || errno == EAGAIN)
		return 0;

	if (VERBOSE && ch == EOF)
		kfprintf(stderr, "Got EOF\n");

	return ch;
}

void gdb_send_packet(char *packet)
{
	char ch;
	char *ptr;
	FILE *fp = rsp.client_fp;
	uint8_t checksum = 0;
	char hex[2] = { 0 };

	fwrite("$", sizeof(char), 1, fp);
	for (ptr = packet; *ptr != 0; ptr++) {
		if (*ptr == '$' || *ptr == '#')
			exit(-1);
		fwrite(ptr, sizeof(char), 1, fp);
		checksum += *ptr;
	}
	fwrite("#", sizeof(char), 1, fp);
	write_hex_byte((char *)&hex, checksum);
	fwrite(hex, sizeof(char), 2, fp);
	if (VERBOSE)
		kfprintf(stderr, "sent: '%s'\n", packet);
	fflush(rsp.client_fp);


	ch = log_getc(fp);
	if (ch != '+')
		kfprintf(stderr, "Unexpected ack char: '%c' (%i)\n", ch, ch);
}

static void gdb_rsp_interrupt_sequence(char *req)
{
	if (VERBOSE)
		kprintf("%s() handling %s\n", __FUNCTION__, req);
	gdb_send_packet("");
}

static void gdb_rsp_enable_extended(char *req)
{
	if (VERBOSE)
		kprintf("%s() handling %s\n", __FUNCTION__, req);
	gdb_send_packet("");
}

static void gdb_rsp_query_status(char *req)
{
	if (VERBOSE)
		kprintf("%s() handling %s\n", __FUNCTION__, req);
	/* We stop due to a TRAP exception */
	gdb_send_packet("S05");
}

static void gdb_rsp_read_reg_single(char *req)
{
	unsigned reg_n, val;
	char msg[8];

	if (VERBOSE)
		kprintf("%s() handling %s\n", __FUNCTION__, req);

	req += 2;

	sscanf(req, "%x", &reg_n);

	if (dbg_read_reg(reg_n, &val) == -1)
		goto exit_error;

	sprintf(msg, "%08x", val);
	gdb_send_packet(msg);

	return;

exit_error:
	gdb_send_packet("E0");
}

static void gdb_rsp_write_reg_single(char *req)
{
	unsigned reg_n, reg_val;
	char *p = req;

	while (*p != '=')
		p++;
	*p = 0;
	sscanf(++req, "%x", &reg_n);
	sscanf(++p, "%x", &reg_val);

	if (VERBOSE)
		kprintf("%s() setting R%d to %x\n", __FUNCTION__, reg_n,
		       reg_val);


	if (dbg_write_reg(reg_n, reg_val) == -1)
		goto exit_error;

	gdb_send_packet("OK");

	return;

exit_error:
	gdb_send_packet("E1");
}

static void gdb_rsp_read_reg_all(char *req)
{
	unsigned i, val;
	char *c;
	char msg[32 * 8];

	if (VERBOSE)
		kprintf("%s() handling %s\n", __FUNCTION__, req);

	for (i = 0, c = msg; i < 32; i++, c += 8) {
		if (dbg_read_reg(i, &val) == -1)
			goto exit_error;
		sprintf(c, "%08x", val);
	}

	gdb_send_packet(msg);

	return;

exit_error:
	gdb_send_packet("E0");
}

static void gdb_rsp_write_reg_all(char *req)
{
	if (VERBOSE)
		kprintf("%s() handling %s\n", __FUNCTION__, req);
	gdb_send_packet("");
}

static void gdb_rsp_write_mem(char *req)
{
	if (VERBOSE)
		kprintf("%s() handling %s\n", __FUNCTION__, req);
	gdb_send_packet("");
}

static void gdb_rsp_read_mem(char *req)
{
	unsigned addr, len, i, data;
	char *msg, *c;

	sscanf(++req, "%x,%x", &addr, &len);

	if (VERBOSE)
		kprintf("%s() reading 0x%x bytes from addr 0x%x \n",
		       __FUNCTION__, len, addr);
	msg = c = malloc(2 * len);

	if (!msg)
		goto exit_error;

	/* FIXME support unaligned reads */
	if (addr & 0x3 || len & 0x3)
		goto exit_error;

	for (i = 0; i < len; i += 4, c += 8) {
		dbg_read_word(addr + i, &data);
		sprintf(c, "%08x", data);
	}

	gdb_send_packet(msg);

	free(msg);

	return;

exit_error:
	gdb_send_packet("E0");
}

static void gdb_rsp_breakpoint(char *req)
{
	if (VERBOSE)
		kprintf("%s() handling %s\n", __FUNCTION__, req);
	gdb_send_packet("");
}

static void gdb_rsp_continue(char *req)
{
	if (VERBOSE)
		kprintf("%s() handling %s\n", __FUNCTION__, req);
	gdb_send_packet("");
}

static void gdb_rsp_single_step(char *req)
{
	if (VERBOSE)
		kprintf("%s() handling %s\n", __FUNCTION__, req);

	if (*(++req) != 0)
		/* FIXME: handle non single step too */
		goto exit_error;

	if (dbg_single_step(1) == -1)
		goto exit_error;

	gdb_send_packet("S05");

	return;

exit_error:
	gdb_send_packet("E0");
}

/* Read a message of the format "$<data>#<checksum>". */
int get_gdb_packet(char *buffer, int buffer_size)
{
	FILE *fp = rsp.client_fp;
	int ret = -1;

	while (!rsp.interrupted) {
		/* Wait for the start character, '$', ignoring others. */

		while (!rsp.interrupted) {
			int ch = log_getc(fp);

			if (rsp.interrupted)
				break;

			if (ch == EOF) {
				ret = -1;
				goto leave;
			}

			if (errno == EWOULDBLOCK || errno == EAGAIN) {
				errno = 0;

				if (VERBOSE)
					kprintf("log_getc timeout\n");

				return 0;
			}

			if (ch == '$')
				break;

			if (ch == '\3') {
				/* Special-case packet : interrupt from client */
				if (buffer_size < 2)
					return -1;

				buffer[0] = ch;
				buffer[1] = '\0';

				ret = 1;

				goto leave;
			}
			if (VERBOSE && ch != '+')
				kfprintf(stderr, "Unexpected char: '%c' (%i)\n",
					ch, ch);
		}
		if (rsp.interrupted) {
			kprintf("Interrupted while reading packet\n");
			goto leave;
		}

		int count = 0;
		uint8_t checksum = 0;
		while (1) {
			if (count >= buffer_size)
				return -1;

			char ch = log_getc(fp);
			if (ch == '#')
				break;
			checksum += ch;
			buffer[count++] = ch;
		}
		buffer[count] = 0;
		uint8_t received_checksum = hex_to_int(log_getc(fp)) << 4;
		received_checksum += hex_to_int(log_getc(fp));
		if (received_checksum != checksum) {
			kfprintf(stderr, "got bad checksum: 0x%02x != 0x%02x\n",
				received_checksum, checksum);
			fwrite("-", sizeof(char), 1, fp);
		} else {
			fwrite("+", sizeof(char), 1, fp);
		}
		fflush(fp);
		if (received_checksum == checksum) {
			if (VERBOSE)
				kfprintf(stderr, "received: '%s'\n", buffer);
			ret = 1;

			goto leave;
		}
	}

leave:
	return ret;
}


void rsp_srv_run()
{
	while (!rsp.interrupted) {
		char req[MAX_DBG_PACKET] = { 0 };
		int rc = get_gdb_packet(req, sizeof(req));

		if (rc == 0) {
			/* timed out while attempting to read packet;
			 * buffer has no content.
			 */
			if (VERBOSE)
				kprintf("Warning: get_gdb_packet timeout "
					"noticed\n");
			continue;
		}

		if (rc == -1) {
			kfprintf(stderr,
				 "%s: Client disconnected or error encountered "
				 "while reading packet.\n", __FUNCTION__);
			break;
		}

		switch (req[0]) {
		case '\3': /* ctrl-C or other interrupt from client */
			gdb_rsp_interrupt_sequence(req);
			break;
		case '!': /* extended mode enabled */
			gdb_rsp_enable_extended(req);
			break;
		case '?': /* query stopped status */
			gdb_rsp_query_status(req);
			break;
		case 'p': /* read SPECIFIC register */
			gdb_rsp_read_reg_single(req);
			break;
		case 'P': /* write SPECIFIC register */
			gdb_rsp_write_reg_single(req);
			break;
		case 'g': /* read registers */
			gdb_rsp_read_reg_all(req);
			break;
		case 'G': /* write registers */
			gdb_rsp_write_reg_all(req);
			break;
		case 'H': /* set thread context */
			gdb_send_packet("");
			break;
		case 'M': /* write memory */
			gdb_rsp_write_mem(req);
			break;
		case 'm': /* read memory */
			gdb_rsp_read_mem(req);
			break;
		case 'Z':
		case 'z':
			gdb_rsp_breakpoint(req);
			break;
		case 'c': /* continue */
			gdb_rsp_continue(req);
			break;
		case 's': /* single step */
			gdb_rsp_single_step(req);
			break;
		default:
			if (VERBOSE)
				kfprintf(stderr,
					"Warning: unhandled packet='%s'\n",
					req);
			gdb_send_packet("");
			break;
		}
	}

	kfprintf(stderr, "%s complete\n", __FUNCTION__);
	return;
}

/*****************/
/* Main function */
/*****************/

int main(int argc, char *argv[])
{
	if (argc != 2) {
		kfprintf(stderr, "Usage: %s <ip of server>.\n", argv[0]);
		return 1;
	}

	if (dbg_init(argv[1]) == -1)
		goto exit_error;

	if (rsp_srv_init() == -1)
		goto exit_error;

	j_state = 0;

	rsp_srv_run();

	return 0;

exit_error:
	kfprintf(stderr, "Error: %s is exiting with error.\n", argv[0]);

	return -1;
}
