%{
#include "kas.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <alloca.h>
#include <stdlib.h>

#define WORD_SIZE               4
#define YYDEBUG                 1
#define TOGGLE_ENDIANESS        1


	int yylex(void);

	enum op {
		OP_LDW = 0x01,
		OP_STW = 0x02,
		OP_LDH = 0x03,
		OP_STH = 0x04,
		OP_LDHU = 0x05,
		OP_STHU = 0x06,
		OP_LDB = 0x07,
		OP_STB = 0x08,
		OP_LDBU = 0x09,
		OP_STBU = 0x0A,
		OP_LDAW = 0x0B,
		OP_STAW = 0x0C,
		OP_MOV = 0x0D,
		OP_NOP = 0x0E,
		OP_MOVI = 0x0F,

		OP_ADD = 0x10,
		OP_SUB = 0x11,
		OP_SHR = 0x12,
		OP_SHL = 0x13,
		OP_NOT = 0x14,
		OP_AND = 0x15,
		OP_OR = 0x16,
		OP_XOR = 0x17,
		OP_MULT = 0x18,
		OP_DIV = 0x19,
		OP_MOD = 0x1A,
		OP_CMPU = 0x1E,
		OP_CMP = 0x1F,

		OP_ADDI = 0x20,
		OP_SUBI = 0x21,
		OP_SHRI = 0x22,
		OP_SHLI = 0x23,
		OP_ANDI = 0x25,
		OP_ORI = 0x26,
		OP_XORI = 0x27,
		OP_MULTI = 0x28,
		OP_DIVI = 0x29,
		OP_MODI = 0x2A,
		OP_CMPUI = 0x2E,
		OP_CMPI = 0x2F,

		OP_JMP = 0x30,
		OP_BO = 0x31,
		OP_BG = 0x32,
		OP_BL = 0x33,
		OP_BEQ = 0x34,
		OP_BEG = 0x36,
		OP_BEL = 0x37,
		OP_BGZ = 0x38,
		OP_BNEQ = 0x39,
		OP_LAST_BR_OP = 0x39,
		OP_JMPR = 0x3D,
		OP_CALL = 0x3E,
		OP_INT = 0x3F,
	};
	enum cmp_flags {
		FLAG_O = 0x1,
		FLAG_G = 0x2,
		FLAG_Q = 0x4,
		FLAG_Z = 0x8,
	};

	FILE *f;
	char out_type = 'h';    /* Default output type is hex */
	unsigned word_offset = 0;
	unsigned n_label = 0;
	unsigned n_pend_label = 0;
	struct {
		unsigned	addr;
		char *		s;
	} label[MAX_LABELS] = { { 0 } };

	struct {
		long		off;
		unsigned	addr;
		unsigned	line;
		char *		s;
	} pend_label[MAX_LABEL_REF];

	/* Given an instruction return the lenght of its immediate in bits. */
	unsigned get_immediate_len(unsigned inst)
	{
		int op = inst >> 26;

		if (OP_LDW <= op && op <= OP_STBU)
			return 16;
		else if (OP_LDAW == op || OP_STAW == op || OP_MOVI == op)
			return 21;
		else if (OP_ADDI <= op && op <= OP_CMPI)
			return 16;
		else if (OP_JMP <= op && op <= OP_CALL)
			return 26;
		else
			return 0;
	}

	/* Toggle word endianess. */
	unsigned toggle_endianess(unsigned word)
	{
		if (TOGGLE_ENDIANESS) {
			char b[4];

			b[3] = (char)(word >> 0u);
			b[2] = (char)(word >> 8u);
			b[1] = (char)(word >> 16u);
			b[0] = (char)(word >> 24u);

			return *(unsigned *)b;
		} else {
			return word;
		}
	}

	/* Given a word emit it in the correct format. */
	void emit_word(unsigned word)
	{
		switch (out_type) {
		case 'b':
			word = toggle_endianess(word);
			fwrite(&word, WORD_SIZE, 1, f);
			break;
		case 'h':
			fprintf(f, "0x%08x\n", word);
			break;
		}
	}

	/* Emit some data. */
	unsigned emit_data(char *data)
	{
		unsigned i, data_len, emit_words, word;

		data_len = strlen(data) + 1;
		emit_words = (data_len + WORD_SIZE - 1) / WORD_SIZE;

		for (i = 0; i < emit_words; i++) {
			if (data_len >= WORD_SIZE) {
				memcpy(&word, data, WORD_SIZE);
			} else {
				memcpy(&word, data, data_len);
				memset(((char *)&word) + data_len, 0,
				       WORD_SIZE - data_len);
			}
			word = toggle_endianess(word);
			emit_word(word);
			data_len -= WORD_SIZE;
			data += WORD_SIZE;
		}

		return emit_words;
	}
	/* Retrive instruction from file at the current position. */
	unsigned retrive_inst()
	{
		unsigned inst;

		switch (out_type) {
		case 'b':
			fread(&inst, sizeof(inst), 1, f);
			inst = toggle_endianess(inst);
			fseek(f, -sizeof(inst), SEEK_CUR);
			break;
		case 'h': {
			char inst_txt[11];

			fread(&inst_txt, 10, 1, f);
			inst_txt[10] = 0;
			inst = (unsigned)strtoll(inst_txt, NULL, 16);
			fseek(f, -10, SEEK_CUR);
			break;
		}
		}

		return inst;
	}

	/* Return -1 if not present or the label index otherwise */
	int check_label(const char *entry)
	{
		int i;

		for (i = 0; i < n_label; i++)
			if (label[i].s && !strcmp(label[i].s, entry))
				return i;

		return -1;
	}

	/* Define a label */
	void def_label(char *entry)
	{
		int i = check_label(entry);

		if (i != -1) {
			char *tmp = alloca(256);
			snprintf(tmp, 256, "defining label \'%s\' twice.", entry);
			free(entry);
			yyerror(tmp);
		}

		if (n_label == MAX_LABELS) {
			char *tmp = alloca(256);
			snprintf(tmp, 256, "defininig label %s exceeds maximum"
				 " number of labels "
				 "(%d).", entry, MAX_LABELS);
			free(entry);
			yyerror(tmp);
		}
		label[n_label].s = entry;
		label[n_label].addr = word_offset << 2;
		n_label++;
	}

	/* Return -1 if not present or the label's value otherwise */
	unsigned retrive_label(const char *entry)
	{
		int i = check_label(entry);

		if (i == -1)
			return -1;

		return label[i].addr;
	}

	/* Mark a reference to a label that will be filled later */
	void mark_pending_label(char *entry)
	{
		if (n_pend_label == MAX_LABEL_REF) {
			char *tmp = alloca(256);
			snprintf(tmp, 256, "referencing label %s exceeds "
				 "maximum number of label references (%d).",
				 entry, MAX_LABEL_REF);
			free(entry);
			yyerror(tmp);
		}

		pend_label[n_pend_label].s = entry;
		pend_label[n_pend_label].line = line;
		pend_label[n_pend_label].off = ftell(f);
		pend_label[n_pend_label].addr = word_offset;
		if (pend_label[n_pend_label].off == -1)
			yyerror("failing to seek output file, something went wrong.");
		n_pend_label++;
	}

	/* Fix all pending references. */
	void reference_labels()
	{
		unsigned i, inst, imm_size, addr, immediate;
		int abs_mem_off;

		for (i = 0; i < n_pend_label; i++) {
			addr = retrive_label(pend_label[i].s);
			if (addr == -1) {
				char *tmp = alloca(256);
				snprintf(tmp, 256,
					 "Error: unable to reference label "
					 "'%s' at line %d.",
					 pend_label[i].s, pend_label[i].line);
				free_labels();
				yyerror(tmp);
			}
			fseek(f, pend_label[i].off, SEEK_SET);
			inst = retrive_inst();
			imm_size = get_immediate_len(inst);
			if ((OP_JMP <= (inst >> 26) &&
			     (inst >> 26) <= OP_LAST_BR_OP) ||
			    ((inst >> 26) == OP_CALL)) {
				/* Word aligned PC relative addressing */
				addr = addr >> 2;
				addr -= pend_label[i].addr + 1;
				abs_mem_off = abs(addr << 2) >> 2;
			} else if ((inst >> 26) == OP_LDW ||
				   (inst >> 26) == OP_STW ||
				   (inst >> 26) == OP_LDAW ||
				   (inst >> 26) == OP_STAW) {
				/* Word aligned abs addressing */
				addr = addr >> 2;
				abs_mem_off = abs(addr << 2) >> 2;
			} else if ((inst >> 26) == OP_LDH ||
				   (inst >> 26) == OP_STH ||
				   (inst >> 26) == OP_LDHU ||
				   (inst >> 26) == OP_STHU) {
				addr = addr >> 1;
				abs_mem_off = abs(addr << 1) >> 1;
			} else {
				abs_mem_off = abs(addr);
			}
			immediate = (abs_mem_off << (8 * sizeof(inst) - imm_size)) >>
				    (8 * sizeof(inst) - imm_size);
			if (immediate != abs_mem_off) {
				char *tmp = alloca(256);
				snprintf(tmp, 256,
					 "Error: immediate overflow for label "
					 "'%s' at line %d.",
					 pend_label[i].s, pend_label[i].line);
				free_labels();
				yyerror(tmp);
			}
			inst |= (addr << (8 * sizeof(inst) - imm_size)) >>
				(8 * sizeof(inst) - imm_size);

			emit_word(inst);
		}
	}

	/* Flush the content of the tmp file to stdout and clean it. */
	void flush_stdout()
	{
		char inst[12];
		unsigned inst_size = (out_type == 'b') ? 4 : 11;

		if (fseek(f, 0, SEEK_SET)) {
			char *tmp = alloca(256);
			snprintf(tmp, 256, "Error: seeking tmp file failed.");
			yyerror(tmp);
		}
		while (fread(inst, inst_size, 1, f))
			fwrite(inst, inst_size, 1, stdout);
		fclose(f);
	}

	/* Free all the dinamic memory allocated for the labels. */
	void free_labels()
	{
		unsigned i;

		for (i = 0; i < n_label; i++)
			free(label[i].s);
		for (i = 0; i < n_pend_label; i++)
			free(pend_label[i].s);
	}

	%}

/* These declare our output file names. */
%output "kas_parser.c"
%defines "kas_parser.h"
%error-verbose

%union {
	unsigned num;
	char *str;
}


%token '(' ')' '+' '-' '*' '/' '%' ',' ':' T_SHR_I T_SHL_I
%token <num> T_DEC T_HEX T_REG
%token <str> T_LITTERAL T_STRING
%token T_ADD T_SUB T_SHR T_SHL T_NOT T_AND T_OR T_XOR T_CMPU T_CMP T_MULT T_DIV
%token T_MOD
%token T_ADDI T_SUBI T_SHRI T_SHLI T_ANDI T_ORI T_XORI T_CMPUI T_CMPI T_MULTI
%token T_DIVI T_MODI
%token T_LDW T_STW T_LDH T_STH T_LDHU T_STHU T_LDB T_STB T_LDBU T_STBU
%token T_MOVI T_LDAW T_STAW T_MOV T_BEQ T_BG T_BL T_BO T_BGZ T_BEG T_BEL T_JMP T_BNEQ
%token T_NOP T_INT T_JMPR T_OFFSET T_CALL T_ASCII T_WORD T_BALIGN T_SECTION

%type <str> label
%type <num> constant numeric immediate rr_op rrr_op rr_inst rrr_inst ri_inst
%type <num> rri_inst r_inst i_inst
%type <num> r_op i_op ri_op rri_w_al_op rri_h_al_op rri_b_al_op rri_no_al_op
%type <num> word offset

%left T_SHR_I T_SHL_I '&' '|' '+' '-' '*' '/' '%'
%right '~' T_OFFSET
%precedence NEG	  /* negation--unary minus */

%%
prog :
| stmt prog;

stmt
: word {
	emit_word($1);
	word_offset++;
}
| T_ASCII T_STRING {
	word_offset += emit_data($2);
}
| T_SECTION T_LITTERAL {
	;
}
| offset {
	word_offset = $1 >> 2;
}
| T_BALIGN immediate {
	if ($2 != WORD_SIZE)
		yyerror("only 4 byte alignment is supported.");
}
| label;

word
: rrr_inst              { $$ = $1; }
| rri_inst              { $$ = $1; }
| rr_inst               { $$ = $1; }
| ri_inst               { $$ = $1; }
| r_inst                { $$ = $1; }
| i_inst                { $$ = $1; }
| T_WORD immediate      { $$ = $2; }
| T_NOP                 { $$ = OP_NOP << 26; }

rrr_inst
: rrr_op T_REG ',' T_REG ',' T_REG {
	$$ = ($1 << 26) | ($2 << 21) | ($4 << 16) | ($6 << 11);
}
| T_CMPU T_REG ',' T_REG {
	$$ = (OP_CMPU << 26) | ($2 << 16) | ($4 << 11);
}
| T_CMP T_REG ',' T_REG {
	$$ = (OP_CMP << 26) | ($2 << 16) | ($4 << 11);
}

rri_inst
: rri_w_al_op T_REG ',' T_REG ',' immediate {
	if ($6 & 0x3)
		yyerror("memory address is not word aligned.");
	$6 = $6 >> 2;
	$$ = ($1 << 26) | ($2 << 21) | ($4 << 16) | ($6 & 0xffff);
}
| rri_h_al_op T_REG ',' T_REG ',' immediate {
	if ($6 & 0x1)
		yyerror("memory address is not half word aligned.");
	$6 = $6 >> 1;
	$$ = ($1 << 26) | ($2 << 21) | ($4 << 16) | ($6 & 0xffff);
}
| rri_b_al_op T_REG ',' T_REG ',' immediate {
	$$ = ($1 << 26) | ($2 << 21) | ($4 << 16) | ($6 & 0xffff);
}
| rri_no_al_op T_REG ',' T_REG ',' immediate {
	$$ = ($1 << 26) | ($2 << 21) | ($4 << 16) | ($6 & 0xffff);
}
| T_CMPUI T_REG ',' immediate {
	$$ = (OP_CMPUI << 26) | ($2 << 16) | ($4 & 0xffff);
}
| T_CMPI T_REG ',' immediate {
	$$ = (OP_CMPI << 26) | ($2 << 16) | ($4 & 0xffff);
}

rr_inst
: rr_op T_REG ',' T_REG {
	$$ = ($1 << 26) | ($2 << 21) | ($4 << 16);
	if ($1 == T_NOT)
		$$ |= ($4 << 11);
}

r_inst
: r_op T_REG {
	$$ = ($1 << 26) | ($2 << 21);
}

ri_inst
: ri_op T_REG ',' immediate {
	if (($1 == OP_LDAW ||
	     $1 == OP_STAW)) {
		if ($4 & 0x3)
			yyerror("memory address is not word aligned.");
		$4 = $4 >> 2;
	}
	$$ = ($1 << 26) | ($2 << 21) | ($4 & ((1 << 21) - 1));
}

i_inst
: i_op immediate {
	if ($1 == OP_INT) {
		$$ = ($1 << 26) | ($2 << 1);
	} else if ($1 == OP_CALL) {
		$$ = ($1 << 26) | $2;
	} else {
		if ($2 & 0x3)
			yyerror("memory address is not word aligned.");
		$$ = ($1 << 26) | (($2 >> 2) & ((1 << 21) - 1));
	}
}

ri_op
: T_MOVI        { $$ = OP_MOVI; }
| T_LDAW        { $$ = OP_LDAW; }
| T_STAW        { $$ = OP_STAW; }

rrr_op
: T_ADD         { $$ = OP_ADD; }
| T_SUB         { $$ = OP_SUB; }
| T_SHR         { $$ = OP_SHR; }
| T_SHL         { $$ = OP_SHL; }
| T_AND         { $$ = OP_AND; }
| T_OR          { $$ = OP_OR; }
| T_XOR         { $$ = OP_XOR; }
| T_MULT        { $$ = OP_MULT; }
| T_DIV         { $$ = OP_DIV; }
| T_MOD         { $$ = OP_MOD; }

rri_w_al_op
: T_LDW         { $$ = OP_LDW; }
| T_STW         { $$ = OP_STW; }

rri_h_al_op
: T_LDH         { $$ = OP_LDH; }
| T_STH         { $$ = OP_STH; }
| T_LDHU        { $$ = OP_LDHU; }
| T_STHU        { $$ = OP_STHU; }

rri_b_al_op
: T_LDB         { $$ = OP_LDB; }
| T_STB         { $$ = OP_STB; }
| T_LDBU        { $$ = OP_LDBU; }
| T_STBU        { $$ = OP_STBU; }

rri_no_al_op
: T_ADDI        { $$ = OP_ADDI; }
| T_SUBI        { $$ = OP_SUBI; }
| T_SHRI        { $$ = OP_SHRI; }
| T_SHLI        { $$ = OP_SHLI; }
| T_ANDI        { $$ = OP_ANDI; }
| T_ORI         { $$ = OP_ORI; }
| T_XORI        { $$ = OP_XORI; }
| T_MULTI       { $$ = OP_MULTI; }
| T_DIVI        { $$ = OP_DIVI; }
| T_MODI        { $$ = OP_MODI; }

rr_op
: T_MOV         { $$ = OP_MOV; }
| T_NOT         { $$ = OP_NOT; }

i_op
: T_JMP         { $$ = OP_JMP; }
| T_BEQ         { $$ = OP_BEQ; }
| T_BG          { $$ = OP_BG; }
| T_BL          { $$ = OP_BL; }
| T_BO          { $$ = OP_BO; }
| T_BGZ         { $$ = OP_BGZ; }
| T_BEG         { $$ = OP_BEG; }
| T_BEL         { $$ = OP_BEL; }
| T_BNEQ        { $$ = OP_BNEQ; }
| T_INT         { $$ = OP_INT; }
| T_CALL        { $$ = OP_CALL; }

r_op
: T_JMPR        { $$ = OP_JMPR; }

immediate
: numeric       { $$ = $1; }
| T_LITTERAL {
	mark_pending_label($1);
	$$ = 0;
}

numeric
: numeric '+' numeric           { $$ = $1 + $3; }
| numeric '-' numeric           { $$ = $1 - $3; }
| numeric '*' numeric           { $$ = $1 * $3; }
| numeric '/' numeric           { $$ = $1 / $3; }
| numeric '%' numeric           { $$ = $1 % $3; }
| numeric '&' numeric           { $$ = $1 & $3; }
| numeric '|' numeric           { $$ = $1 | $3; }
| numeric '~' numeric           { $$ = ~$3; }
| numeric T_SHR_I numeric       { $$ = $1 >> $3; }
| numeric T_SHL_I numeric       { $$ = $1 << $3; }
| '-' numeric %prec NEG         { $$ = -$2; }
| '(' numeric ')'               { $$ = $2; }
| constant                      { $$ = $1; }

constant
: T_HEX                         { $$ = $1; }
| T_DEC                         { $$ = $1; }

label
: T_LITTERAL ':'                { def_label($1); }

offset
: T_OFFSET numeric              { $$ = $2; }

%%

int main(int argc, char *argv[])
{
	/* yydebug = 1; */
	if (argc > 1) {
		if (argc > 2 || argv[1][0] != '-' ||
		    (argv[1][1] != 'h' && argv[1][1] != 'b')) {
			printf("KAS an assembler for KPU.\n"
			       "Allowed options are:\n"
			       "\t--help\tShows this message.\n"
			       "\t-h\tHex output.\n"
			       "\t-b\tBinary output.\n");

			return 1;
		}
		out_type = argv[1][1];
	}
	f = tmpfile();
	if (!f) {
		fprintf(stderr, "Error: unable to open temporally file.");
		goto exit_error;
	}
	if (yyparse())
		goto exit_error;
	reference_labels();
	flush_stdout();

	return 0;
exit_error:
	free_labels();

	return 1;
}
