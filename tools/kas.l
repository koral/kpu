
%{
#include <stdarg.h>
#include "kas.h"
#include "kas_parser.h"

#define YY_NO_INPUT

unsigned line = 1;

char *str_cp (const char *in)
{
	size_t len = strnlen(in, MAX_LABEL_LEN);
	char* buf = malloc(len + 1);
	strncpy(buf, in, len);
	buf[len] = '\0';

	return buf;
}

%}

%option outfile="kas_lexer.c" header-file="kas_lexer.h"
%option noyywrap
%option nounput

HEX      0[Xx][0-9A-Fa-f]+
REG      [Rr][0-9]+
DEC	 [0-9]+
COMMENT  ";"[^\r\n]*
WS       [ \t]*
NLINE    [\r\n]
LITTERAL [$.0-9A-Za-z_]+
STRING   \"([^\\\"]|\\.)*\"

%%

{WS}		; /* skip whitspace */
{COMMENT}	; /* skip comments */
{NLINE}	        line++;
"#pragma offset"	{ return T_OFFSET; }
".ascii"	{ return T_ASCII; }
".word"		{ return T_WORD; }
".balign"	{ return T_BALIGN; }
".section"	{ return T_SECTION; }
{REG}		{
			sscanf(yytext + 1, "%d", &yylval.num);
		  	if (yylval.num > 31)
				yyerror("Invalid register number.");
			else
				return T_REG;
		}

FP|fp		{
			yylval.num = 26;
			return T_REG;
}

SP|sp		{
			yylval.num = 27;
			return T_REG;
}

PC|pc		{
			yylval.num = 31;
			return T_REG;
}

SR|sr		{
			yylval.num = 30;
			return T_REG;
}

{HEX}		{
			sscanf(yytext, "%x", &yylval.num);
			return T_HEX;
		}
{DEC}		{
			sscanf(yytext, "%d", &yylval.num);
			return T_DEC;
		}
{STRING}	{
			int i, j;

			for(i = 0; i < yyleng ; ++i) {
				if (yytext[i] == '\\' && yytext[i + 1] == '\"') {
					j = i;
					for( ; j < yyleng ; ++j )
						yytext[j] = yytext[j + 1];
					yyleng--;
					}
				}
			yylval.str = strdup(yytext + 1);
			if (yylval.str[yyleng - 2] != '"')
				yyerror("Improperly terminated string.");
			else
				yylval.str[yyleng - 2] = 0;
			return T_STRING;
		}

ADD|add		return T_ADD;
SUB|sub		return T_SUB;
SHR|shr		return T_SHR;
SHL|shl		return T_SHL;
NOT|not		return T_NOT;
AND|and		return T_AND;
OR|or		return T_OR;
XOR|xor		return T_XOR;
CMPU|cmpu	return T_CMPU;
CMP|cmp		return T_CMP;
MULT|mult	return T_MULT;
DIV|div		return T_DIV;
MOD|mod		return T_MOD;
ADDI|addi	return T_ADDI;
SUBI|subi	return T_SUBI;
SHRI|shri	return T_SHRI;
SHLI|shli	return T_SHLI;
ANDI|andi	return T_ANDI;
ORI|ori		return T_ORI;
XORI|xori	return T_XORI;
CMPUI|cmpui	return T_CMPUI;
CMPI|cmpi	return T_CMPI;
MULTI|multi	return T_MULTI;
DIVI|divi	return T_DIVI;
MODI|modi	return T_MODI;
LDW|ldw		return T_LDW;
STW|stw		return T_STW;
LDH|ldh		return T_LDH;
STH|sth		return T_STH;
LDB|ldb		return T_LDB;
STB|stb		return T_STB;
LDHU|ldhu	return T_LDHU;
STHU|sthu	return T_STHU;
LDBU|ldbu	return T_LDBU;
STBU|stbu	return T_STBU;
LDAW|ldaw	return T_LDAW;
STAW|staw	return T_STAW;
MOV|mov		return T_MOV;
MOVI|movi	return T_MOVI;
JMP|jmp		return T_JMP;
BO|bo		return T_BO;
BG|bg		return T_BG;
BL|bl		return T_BL;
BEQ|beq		return T_BEQ;
BEG|beg		return T_BEG;
BEL|bel		return T_BEL;
BGZ|bgz		return T_BGZ;
NOP|nop		return T_NOP;
BNEQ|bneq	return T_BNEQ;
INT|int		return T_INT;
CALL|call	return T_CALL;
JMPR|jmpr	return T_JMPR;

"("		return '(';
")"		return ')';
"+"		return '+';
"-"		return '-';
"*"		return '*';
"/"		return '/';
"%"		return '%';
">>"		return T_SHR_I;
"<<"		return T_SHL_I;
"~"		return '~';
"&"		return '&';
"|"		return '|';
","		return ',';
":"		return ':';

{LITTERAL}      {
 		       if (strlen(yytext) > MAX_LABEL_LEN)
				yyerror("litteral exceding max length");
			yylval.str = str_cp(yytext);
			return T_LITTERAL;
		}

.|\n		{
			yyerror("unrecognized character %c", *yytext);
		}

%%

int yyerror(const char* format, ...)
{
	va_list args;

    	fflush(stdout);

	fprintf(stderr, "Error: ");
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
	fprintf(stderr, " at line %d\n", line);

	fflush(stderr);
	free_labels();

	exit(1);
}
