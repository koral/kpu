(defun kpu-cleanup-log ()
  "Cleanup a kpu tb log buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward (rx (seq
				   line-start
				   (zero-or-more nonl)
				   (or "Stop"
				       "Reset"
				       "Branching PC")
				   (zero-or-more nonl)
				   line-end))
			      nil t)
      (kill-region (match-beginning 0) (1+ (match-end 0))))))

(defun kpu-clk-prune ()
  "Remove any time based information form a log buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward (rx (seq
				   line-start
				   (zero-or-more nonl)
				   (group-n 1
					    (seq "clk "
						 (one-or-more digit)
						 ":"))
				   (zero-or-more nonl)
				   line-end))
			      nil t)
      (kill-region (match-beginning 1) (match-end 1)))))

(provide 'kpu)
