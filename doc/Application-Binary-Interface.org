* ABI:
** Data types:

   The data types used by KPU programs are shown in the following table.
   data32, data16 and data8 correspond to word, half word and byte.

   | KPU data type | ANSI C type | Size (byte) |
   |---------------+-------------+-------------|
   | data8         | char        |           1 |
   | data16        | short       |           2 |
   | data32        | int         |           4 |
   | data32        | long int    |           4 |
   | data32        | enum        |           4 |
   | data32        | pointer     |           4 |

** KCC ABI
*** Register Usage Conventions:

   The register usage convention for KPU is given in the following table:

   | Register | Enforcement | Purpose                                     |
   |----------+-------------+---------------------------------------------|
   | R0       | SW          | Return value                                |
   | R1-R24   | SW          | Passing parameters / Temporaries            |
   | R25      | SW          | Assumed to be zeroed                        |
   | R26(FP)  | SW          | Frame pointer                               |
   | R27(SP)  | SW          | Stack pointer                               |
   | R28      | HW          | Procedure return address                    |
   | R29      | HW          | Interrupt / exception / trap return address |
   | R30 (SR) | HW          | Machine configuration / status register     |
   | R31 (PC) | HW          | Program counter                             |

-  all temporaries register are callee save.
-  arguments are passed using Temporaries register starting from R1.

*** Stack convention

   The stack conventions used by kcc are detailed in the following table (where n is the number of temporary register used by the current procedure):

   | Address          | Content                                                                                                    |
   |------------------+------------------------------------------------------------------------------------------------------------|
   | FP (old SP)      | Caller Base Pointer                                                                                        |
   | FP - 4           | Current procedure Return Address                                                                           |
   | FP - 8           | R25 first Callee Saved Registers (only those registers which are used by the current procedure are saved)  |
   | FP - 12          | R24 second Callee Saved Registers (only those registers which are used by the current procedure are saved) |
   | ...              | ...                                                                                                        |
   | FP - 4 * (n + 1) | Last Callee Saved Registers (only those registers which are used by the current procedure are saved)       |
   | SP               |                                                                                                            |


** GCC ABI
*** Register Usage Conventions:

   The register usage convention for KPU is given in the following table:

   | Register | Type         | Enforcement | Purpose                                     |
   |----------+--------------+-------------+---------------------------------------------|
   | R0       | Dedicated    | SW          | Return value / Temporaries                  |
   | R1-R10   | Volatile     | SW          | Passing parameters / Temporaries            |
   | R11-R12  | Volatile     | SW          | Temporaries                                 |
   | R15-R25  | Non volatile | SW          | Must be saved across function calls         |
   | R26(FP)  | Dedicated    | SW          | Assembler reserved                          |
   | R27(SP)  | Dedicated    | SW          | Stack pointer                               |
   | R28      | Dedicated    | SW          | Procedure return address                    |
   | R29      | Dedicated    | HW          | Interrupt / exception / trap return address |
   | R30 (SR) | Dedicated    | HW          | Machine configuration / status register     |
   | R31 (PC) | Dedicated    | HW          | Program counter                             |

-  volatile registers are used for temporaries and do not retain their
   value across function calls.
-  non volatile registers retain their contents across function calls.
   The callee function is expected to save those non volatile registers
   typically during the prologue end restore it during the epilogue.

*** Stack convention

   The stack conventions used by kcc are detailed in the following table (where n is the number of temporary register used by the current procedure):

   | Address     | Content                          |
   |-------------+----------------------------------|
   | FP + 8      | Current procedure Return Address |
   | FP + 4      | Caller Base Pointer              |
   | FP (old SP) | First Callee Saved Registers     |
   | FP - 8      | Second Callee Saved Registers    |
   | FP - 12     |                                  |
   | ...         | ...                              |
   | FP - 4 * n  | Last Callee Saved Registers      |
   | SP          |                                  |
