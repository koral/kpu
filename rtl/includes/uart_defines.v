`ifndef __DEVICE_TABLE_DEFINES__
`define __DEVICE_TABLE_DEFINES__

//project_defines.v
`define DRT_NUM_OF_DEVICES 1
`define DRT_INPUT_FILE "drt_rom_file.txt"
`define SIM

`define	UART_REG_CONTROL	32'h00000000
`define	UART_REG_STATUS		32'h00000001
`define	UART_REG_PRESCALER	32'h00000002
`define	UART_REG_CLOCK_DIV	32'h00000003
`define	UART_REG_WRITE_COUNT	32'h00000004
`define	UART_REG_WRITE		32'h00000005
`define	UART_REG_READ_COUNT	32'h00000006
`define	UART_REG_READ		32'h00000007

`endif
