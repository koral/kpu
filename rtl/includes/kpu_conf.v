///////////////////////////////////////////////////
//-----------------------------------------------------------------------------
// Title         : KPU configuration file
// Project       : KPU
//-----------------------------------------------------------------------------
// File          : kpu_conf.v.v
// Author        : acorallo  <andrea_corallo@yahoo.it>
// Created       : 17.12.2016
//-----------------------------------------------------------------------------
// Description :
// Configuration file for KPU
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2016 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 17.12.2016 : created
//-----------------------------------------------------------------------------

`ifndef _kpu_conf
 `define _kpu_conf

 `define N		32		// cpu word size
 `define REG_N		32 	  	// 32 general purpose regs
 `define REG_NBIT	5		// 5 bits are needed to encode them
 `define RAM_SIZE	(1 << 10)	// 1K words => 4KB on board memory
 `define ROM_SIZE	(1 << 10)	// 1K words => 4KB on board read only memory
 `define PC_N		31		// R31 register is used as PC
 `define SR_N		30		// R30 register is used as SR
 `define RET_INT_N	29		// When interrupt is taken ret addr is saved into R29
 `define RET_PROC_N	28		// When a procedure is called ret addr is saved into R28
 `define IO_INT_N	16		// Max number of external IO interrupts

/////////////////////////////////////////////
// Default clock rate (can be overwritten) //
/////////////////////////////////////////////
`ifndef CLOCK_RATE
 `define CLOCK_RATE 33_000_000
`endif

 /////////////////////////////
 // Memory mapped addresses //
 /////////////////////////////

 ///////////////////////////////////
 // External SRAM initial address //
 ///////////////////////////////////
`define EXT_SRAM_ADDR	32'h4000000
`define EXT_SRAM_MASK	(`EX_SRAM_ADDR - 1)

 ////////////////////////
 // ROM intial address //
 ////////////////////////
 `define ROM_ADDR	32'hffff8000


 /////////////////////////////////////////////
 // Interrupt Controller / Interrupt vector //
 /////////////////////////////////////////////

 `define RESET_INT_VADDR	32'h00000000	// Reset interrupt vector addr
 `define IO_INT_VADDR		32'h00000008	// IO interrupt interrupt vector addr
 `define INVALID_OP_INT_VADDR	32'h00000010	// Invalid OP interrupt vector addr
 `define ALU_EX_INT_VADDR	32'h00000018	// ALU exception interrupt vector addr
 `define MEM_ALIGN_INT_VADDR	32'h00000020	// Memory alignment exception interrupt vector addr
 `define STORE_INT_VADDR	32'h00000028	// Signed store overflow interrupt vector addr

 `define IO_INT_NUM_MAP	32'hfffffffc	// Raised IO interrupt number addr

 //////////////////////////////////
 // GPIO memory mapped registers //
 //////////////////////////////////
 `define GP_OUT_MAP	32'hfffffff8	// GPOUT mapping address
 `define GP_IN_MAP	32'hfffffff4	// GPOIN mapping address

 //////////////////////////////////
 // UART memory mapped registers //
 //////////////////////////////////
 `define UART_CONTROL_REG_MAP		32'hffffffec
 `define UART_STATUS_REG_MAP		32'hffffffe8
 `define UART_PRESCALER_REG_MAP		32'hffffffe4
 `define UART_CLOCK_DIV_REG_MAP		32'hffffffe0
 `define UART_WRITE_COUNT_REG_MAP	32'hffffffdc
 `define UART_WRITE_REG_MAP		32'hffffffd8
 `define UART_READ_COUNT_REG_MAP	32'hffffffd4
 `define UART_READ_REG_MAP		32'hffffffd0


 //////////////////////////////
 // KPU OP codes definitions //
 //////////////////////////////

 `define LDW_OP	  6'h01
 `define STW_OP	  6'h02
 `define LDH_OP	  6'h03
 `define STH_OP	  6'h04
 `define LDHU_OP  6'h05
 `define STHU_OP  6'h06
 `define LDB_OP	  6'h07
 `define STB_OP	  6'h08
 `define LDBU_OP  6'h09
 `define STBU_OP  6'h0A
 `define LDAW_OP  6'h0B
 `define STAW_OP  6'h0C
 `define MOV_OP	  6'h0D
 `define NOP_OP	  6'h0E
 `define MOVI_OP  6'h0F
 `define ADD_OP	  6'h10
 `define SUB_OP	  6'h11
 `define SHR_OP	  6'h12
 `define SHL_OP	  6'h13
 `define NOT_OP	  6'h14
 `define AND_OP	  6'h15
 `define OR_OP	  6'h16
 `define XOR_OP	  6'h17
 `define MULT_OP  6'h18
 `define DIV_OP	  6'h19
 `define MOD_OP	  6'h1A
 `define CMPU_OP  6'h1E
 `define CMP_OP	  6'h1F
 `define ADDI_OP  6'h20
 `define SUBI_OP  6'h21
 `define SHRI_OP  6'h22
 `define SHLI_OP  6'h23
 `define ANDI_OP  6'h25
 `define ORI_OP	  6'h26
 `define XORI_OP  6'h27
 `define MULTI_OP 6'h28
 `define DIVI_OP  6'h29
 `define MODI_OP  6'h2A
 `define CMPUI_OP 6'h2E
 `define CMPI_OP  6'h2F
 `define JMP_OP	  6'h30
 `define BO_OP	  6'h31
 `define BG_OP	  6'h32
 `define BL_OP	  6'h33
 `define BEQ_OP	  6'h34
 `define BEG_OP	  6'h36
 `define BEL_OP	  6'h37
 `define BGZ_OP	  6'h38
 `define BNEQ_OP  6'h39
 `define JMPR_OP  6'h3D
 `define CALL_OP  6'h3E
 `define INT_OP   6'h3F

`endif // `ifndef _kpu_conf
