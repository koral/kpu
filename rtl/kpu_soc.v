//-----------------------------------------------------------------------------
// Title         : KPU main test bench file
// Project       : KPU
//-----------------------------------------------------------------------------
// File          : kpu_soc.v
// Author        : acorallo  <andrea_corallo@yahoo.it>
// Created       : 17.12.2016
//-----------------------------------------------------------------------------
// Description :
// Main file for KPU SoC
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2016 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 17.12.2016 : created
// 04.04.2017 : renamed kpu_soc.v
//-----------------------------------------------------------------------------
`timescale 1ns/1ps
`include "kpu_conf.v"
`include "kcache_defs.v"

module kpu_soc(
	       input 			      CLK100MHZ,
	       input 			      rst_ext,
`ifdef KPU_SIM_TRACE
	       input 			      stop_debug,
	       input [`IO_INT_N-1:0] 	      io_int_line_debug,
`endif
	       // IO ports
	       input 			      uart_rx,
	       output 			      uart_tx,
	       output reg [7:0] 	      ja,
	       // External SRAM interface
	       output wire [`SRAM_ADDR_W-1:0] sram_addr_o,
	       inout wire [`SRAM_DATA_W-1:0]  sram_data_io,
	       output wire 		      sram_cs_o,
	       output wire 		      sram_we_o,
	       output wire 		      sram_oe_o,
	       output wire 		      sram_hb_o,
	       output wire 		      sram_lb_o,
	       // JTAG
	       input 			      tms_pad_i, // JTAG test mode select pad
	       input 			      tck_pad_i, // JTAG test clock pad
	       input 			      trstn_pad_i, // JTAG test reset pad
	       input 			      tdi_pad_i, // JTAG test data input pad
	       output 			      tdo_pad_o, // JTAG test data output pad
	       output 			      tdo_padoe_o // JTAG test data output enable pad
	       );
`ifndef KPU_SIM_TRACE
	reg [`IO_INT_N-1:0] 	io_int_line_debug  = `IO_INT_N'h0;
	reg 			stop_debug = 1'b0;
`endif
	wire 			sys_clk;
	wire 			kpu_clk;
	wire 			debug_mode;
	reg 			rst;
	reg 			rst_pw_on;
	reg [31:0] 		pw_on_cnt;
	reg [`IO_INT_N-1:0] 	io_int_line;
	wire 			mem_wait;
	wire 			io_int;
	wire 			int_ctrl_rst;
	wire [`N-1:0] 		io_int_num;
	wire [`N-1:0] 		kpu_gpo;
	wire 			kpu_stop;
	wire 			wb_int_send;
	wire 			rts;
	wire 			cts;
	// memory lines
	wire [`N-1:0] 		inst_addr;
	wire [`N-1:0] 		kpu_inst_data;
	wire [`N-1:0] 		mem_inst_data;
	wire [`N-1:0] 		data_addr;
	wire [3:0] 		kpu_data_b_sel;
	wire [3:0] 		mem_data_b_sel;
	wire [`N-1:0] 		kpu_data_o;
	wire [`N-1:0] 		mem_data_o;
	wire [`N-1:0] 		kpu_data_i;
	wire [`N-1:0] 		mem_data_i;
	// wishbone bridge lines
	wire 			wb_i_ready;
	wire [31:0] 		wb_i_command;
	wire [31:0] 		wb_i_address;
	wire [31:0] 		wb_i_data;
	wire [27:0] 		wb_i_data_count;
	wire [31:0] 		wb_o_status;
	wire 			wb_o_en;
	wire [31:0] 		wb_o_address;
	wire [31:0] 		wb_o_data;
	wire [27:0] 		wb_o_data_count;
	// wishbone bus lines
	wire 			wb_we_send;
	wire 			wb_cyc_send;
	wire [3:0] 		wb_sel_send;
	wire [31:0] 		wb_dat_send;
	wire 			wb_stb_send;
	wire 			wb_ack_recv;
	wire [31:0] 		wb_dat_recv;
	wire [31:0] 		wb_adr_send;

	always @(*) begin
		ja 		= kpu_gpo[7:0];
`ifdef KPU_SIM_TRACE
		io_int_line 	= io_int_line_debug;
`endif
		io_int_line[0] 	= wb_int_send;
	end

`ifdef KPU_SIM_TRACE
	assign sys_clk  = CLK100MHZ;
`else
	clk_wiz_0 clk_wiz_i(.clk_out1(sys_clk), // output clk_out1
			    .reset(1'b0), // input reset
			    .locked(lk), // output locked
			    .clk_in1(CLK100MHZ)); // input clk_in1
`endif
	initial begin
		rst_pw_on  = 1;
		pw_on_cnt  = 1;
	end

	always @(posedge sys_clk) begin
		if (pw_on_cnt[31])
			rst_pw_on <= 0;
		else
			pw_on_cnt <= pw_on_cnt << 1;
	end

	always @(*)
		rst = rst_ext | rst_pw_on;


	kpu kpu_i(.clk_i(kpu_clk),
		  .rst_i(rst),
		  .stop_i(kpu_stop),
		  .io_int_i(io_int),
		  .inst_addr_o(inst_addr),
		  .inst_data_i(kpu_inst_data),
		  .data_addr_o(data_addr),
		  .data_b_sel_o(kpu_data_b_sel),
		  .data_o(kpu_data_o),
		  .data_i(kpu_data_i),
		  .debug_i(debug_mode));

	dgb dgb_i(.clk_i(sys_clk),
		  .debug_o(debug_mode),
		  // JTAG
		  .tck_pad_i(tck_pad_i),
		  .tms_pad_i(tms_pad_i),
		  .tdi_pad_i(tdi_pad_i),
		  .tdo_pad_o(tdo_pad_o),
		  .tdo_padoe_o(tdo_padoe_o),
		  .trstn_pad_i(trstn_pad_i),
		  // From to KPU
		  .kpu_clk_o(kpu_clk),
		  .kpu_wait_o(kpu_stop),
		  .kpu_d_sel_i(kpu_data_b_sel),
		  .kpu_d_data_i(kpu_data_o),
		  .kpu_i_data_o(kpu_inst_data),
		  .kpu_d_data_o(kpu_data_i),
		  // From to mem
		  .mem_kpu_wait_i(mem_wait || stop_debug),
		  .mem_d_sel_o(mem_data_b_sel),
		  .mem_i_data_i(mem_inst_data),
		  .mem_d_data_i(mem_data_i),
		  .mem_d_data_o(mem_data_o));

	mem mem_i(.clk_i(sys_clk),
		  .kpu_wait_o(mem_wait),
		  .i_addr_i(inst_addr),
		  .i_data_o(mem_inst_data),
		  .d_addr_i(data_addr),
		  .d_sel_i(mem_data_b_sel),
		  .d_data_i(mem_data_o),
		  .d_data_o(mem_data_i),
		  .d_gp_i(0),
		  .d_gp_o(kpu_gpo),
		  .io_int_num_i(io_int_num),
		  .int_ctrl_rst_o(int_ctrl_rst),
		  .wb_ready_o(wb_i_ready),
		  .wb_command_o(wb_i_command),
		  .wb_addr_o(wb_i_address),
		  .wb_data_o(wb_i_data),
		  .wb_data_count_o(wb_i_data_count),
		  .wb_data_count_i(wb_o_data_count),
		  .wb_status_i(wb_o_status),
		  .wb_data_i(wb_o_data),
		  .wb_o_en_i(wb_o_en),
		  .sram_addr_o(sram_addr_o),
		  .sram_data_io(sram_data_io),
		  .sram_cs_o(sram_cs_o),
		  .sram_we_o(sram_we_o),
		  .sram_oe_o(sram_oe_o),
		  .sram_hb_o(sram_hb_o),
		  .sram_lb_o(sram_lb_o)
		  );

	int_ctrl int_ctrl_i(.clk_i(sys_clk),
			    .rst_i(int_ctrl_rst),
			    .io_int_i(io_int_line),
			    .io_int_o(io_int),
			    .int_num_o(io_int_num));

	wishbone_master wb_m_i(.clk(sys_clk),
			       .rst(rst),
			       .i_ih_rst(rst),
			       .i_ready(wb_i_ready),
			       .i_command(wb_i_command),
			       .i_address(wb_i_address),
			       .i_data(wb_i_data),
			       .i_data_count(wb_i_data_count),
			       .i_out_ready(1'b1),
			       .o_en(wb_o_en),
			       .o_status(wb_o_status),
			       .o_address(wb_o_address),
			       .o_data(wb_o_data),
			       .o_data_count(wb_o_data_count),
			       // wishbone bus
			       .o_per_adr(wb_adr_send),
			       .o_per_dat(wb_dat_send),
			       .i_per_dat(wb_dat_recv),
			       .o_per_stb(wb_stb_send),
			       .o_per_cyc(wb_cyc_send),
			       .o_per_we(wb_we_send),
			       .o_per_msk(),
			       .o_per_sel(wb_sel_send),
			       .i_per_ack(wb_ack_recv),
			       .i_per_int(1'b0),
			       .i_mem_dat(`N'b0),
			       .i_mem_ack(1'b0),
			       .i_mem_int(1'b0));

	wb_uart wb_uart_i(.clk(sys_clk),
			  .rst(rst),
			  .i_wbs_we(wb_we_send),
			  .i_wbs_cyc(wb_cyc_send),
			  .i_wbs_sel(wb_sel_send),
			  .i_wbs_dat(wb_dat_send),
			  .i_wbs_stb(wb_stb_send),
			  .o_wbs_ack(wb_ack_recv),
			  .o_wbs_dat(wb_dat_recv),
			  .i_wbs_adr(wb_adr_send),
			  .o_wbs_int(wb_int_send),
			  .o_tx(uart_tx),
			  .i_rx(uart_rx),
			  .i_rts(rts),
			  .o_cts(cts),
			  .i_dtr(1'b0));

endmodule // kpu_test
