`timescale 1ns / 1ps
//-----------------------------------------------------------------------------
// Title	 : KPU top file for digilent arty board simulation
// Project	 : KPU
//-----------------------------------------------------------------------------
// File		 : kpu.v
// Author	 : acorallo  <andrea_corallo@yahoo.it>
// Created	 : 22.1.2017
//-----------------------------------------------------------------------------
// Description :
// Top module for kpu implementation on digilent arty development simulation
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2016 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 22.1.2017 : created
// 4.4.2017 :  use for both vivado and iverilog
//-----------------------------------------------------------------------------
`include "kpu_conf.v"
`include "kcache_defs.v"

`define EXEC_INSTS(i) \
#(((i) + 5) * 10);

module top_kpu_sim();
	reg clk_sim;
	reg rst;
	reg stop;
	wire uart_rx;
	wire uart_tx;
	wire [7:0] ja;
	reg [`IO_INT_N-1:0] io_int_line  = `IO_INT_N'h0;
	// sram lines
	wire [`SRAM_ADDR_W-1:0]  sram_addr;
	wire [`SRAM_DATA_W-1:0] sram_data;
	wire 			 sram_cs;
	wire 			 sram_we;
	wire 			 sram_oe;
	wire 			 sram_hb;
	wire 			 sram_lb;
	// uart test lines
	wire              cts;
	reg               test_transmit = 0;
	reg [7:0] 	  test_tx_byte  = 8'h0;
	wire              test_received;
	wire [7:0] 	  test_rx_byte;
	wire              test_is_receiving;
	wire              test_is_transmitting;
	wire              test_rx_error;
	wire 		  wb_int_send;
	wire 		  tms;
	wire 		  tck;
	wire 		  trst;
	wire 		  tdi;
	wire 		  tdo;
	wire 		  tdo_oe;

	initial begin
		rst 	 = 1;
		stop 	 = 0;
		clk_sim  = 0;
		#10rst 	 = 0;
		#310; // First 32 cycles are internal reset

		// We wait to see two time 0 on sram ext addr line
		// This means kcache has finished his initial job
		while (sram_addr != 0)
			#1;
		#100;
		while (sram_addr != 0)
			#1;

`ifdef DUMP_FILE
		$dumpfile(`DUMP_FILE);
		$dumpvars;
`endif

`ifdef TEST_PLAN
 `include `TEST_PLAN
		$finish;
`endif
	end // initial begin

	always #5 clk_sim <= !clk_sim;

	kpu_soc kpu_soc_i(
`ifdef KPU_SIM_TRACE
			  .stop_debug(stop),
			  .io_int_line_debug(io_int_line),
`endif
			  .CLK100MHZ(clk_sim),
			  .rst_ext(rst),
			  .ja(ja),
			  .uart_tx(uart_tx),
			  .uart_rx(uart_rx),
			  // SRAM
			  .sram_addr_o(sram_addr),
			  .sram_data_io(sram_data),
			  .sram_cs_o(sram_cs),
			  .sram_we_o(sram_we),
			  .sram_oe_o(sram_oe),
			  .sram_hb_o(sram_hb),
			  .sram_lb_o(sram_lb),
			  // JTAG
			  .tms_pad_i(tms),
			  .tck_pad_i(tck),
			  .trstn_pad_i(trst),
			  .tdi_pad_i(tdi),
			  .tdo_pad_o(tdo),
			  .tdo_padoe_o(tdo_oe)
			  );

	sram sram_i(
		    .addr_i(sram_addr),
		    .data_io(sram_data),
		    .CS_i(sram_cs),
		    .WE_i(sram_we),
		    .OE_i(sram_oe),
		    .HB_i(sram_hb),
		    .LB_i(sram_lb)
		    );

	v_jtag v_jtag_i(
			.clk_i(clk_sim),
			.tms_o(tms),
			.tck_o(tck),
			.trst_o(trst),
			.tdi_o(tdi),
			.tdo_i(tdo),
			.tdo_oe_i(tdo_oe)
			);

	uart_v3 u_test_i (.clk(clk_sim),
			  .rst(rst),
			  .rx(uart_tx),
			  .tx(uart_rx),
			  .transmit(test_transmit),
			  .tx_byte(test_tx_byte),
			  .received(test_received),
			  .rx_byte(test_rx_byte),
			  .is_receiving(test_is_receiving),
			  .is_transmitting(test_is_transmitting),
			  .rx_error(test_rx_error),
			  .set_clock_div(1'b0),
			  .user_clock_div(`N'b0));
endmodule
