`timescale 1ns/1ps
//-----------------------------------------------------------------------------
// Title	 : ALU
// Project	 : KPU
//-----------------------------------------------------------------------------
// File		 : alu.v
// Author	 : acorallo  <andrea_corallo@yahoo.it>
// Created	 : 17.12.2016
//-----------------------------------------------------------------------------
// Description :
// Arithmetic logic implementation for KPU
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2016 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 17.12.2016 : created
//-----------------------------------------------------------------------------

`ifndef _alu
 `define _alu
 `include "kpu_conf.v"
 `include "uart_defines.v"

module alu(input wire  [3:0]	alu_op_i,
	   input wire  [`N-1:0] a_i,
	   input wire  [`N-1:0] b_i,
	   output reg  [`N-1:0] out_o,
	   output wire [3:0] flags_o);

	wire [`N-1:0]		add_r;
	wire [`N-1:0]		sub_r;
	wire			oflow_add;
	wire			oflow_sub;
	wire signed [`N-1:0]	a_signed;
	wire signed [`N-1:0]	b_signed;
	wire			signed_op;
	reg			ovr_flag;

	assign a_signed	 = a_i;
	assign b_signed	 = b_i;

	assign add_r = a_i + b_i;
	assign sub_r = a_i - b_i;

	assign oflow_add = (a_i[`N-1] == b_i[`N-1] && add_r[`N-1] != a_i[`N-1]) ?
			   1 : 0;
	assign oflow_sub = (a_i[`N-1] == b_i[`N-1] && sub_r[`N-1] != a_i[`N-1]) ?
			   1 : 0;

	assign signed_op = alu_op_i != 4'hE; // just cmpu is signed
	assign flags_o	 = signed_op ?
			   { out_o > 0 , a_i == b_i, a_i > b_i, ovr_flag } :
			   { out_o > 0, a_signed == b_signed,
			     a_signed > b_signed, ovr_flag };

	always @(*)
	  case (alu_op_i)
		  4'h0:
		    ovr_flag = oflow_add; // add
		  4'h1:
		    ovr_flag = oflow_sub; // sub
		  default:
		    ovr_flag = 1'b0;
	  endcase // case (alu_op_i)

	always @(*)
	  case (alu_op_i)
		  4'h0: out_o = add_r;		// add
		  4'h1: out_o = sub_r;		// sub
		  4'h2: out_o = a_i >> b_i;	// shr
		  4'h3: out_o = a_i << b_i;	// shl
		  4'h4: out_o = ~a_i;		// not
		  4'h5: out_o = a_i & b_i;	// and
		  4'h6: out_o = a_i | b_i;	// or
		  4'h7: out_o = a_i ^ b_i;	// xor
		  4'h8: out_o = a_i * b_i;	// mult
		  4'h9: out_o = a_i / b_i;	// div
		  4'hA: out_o = a_i % b_i;	// mod
		  4'hE, 4'hF: out_o = a_i;	// cmpu, cmp
		  default: out_o = `N'h0;	// latch prevention
	  endcase // case (alu_op_i)

endmodule // alu


`endif
