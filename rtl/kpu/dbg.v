`timescale 1ns/1ps
//-----------------------------------------------------------------------------
// Title	 : DBG
// Project	 : KPU
//-----------------------------------------------------------------------------
// File		 : dbg.v
// Author	 : acorallo  <andrea_corallo@yahoo.it>
// Created	 : 08.06.2017
//-----------------------------------------------------------------------------
// Description :
// Debug support logic implementation for KPU
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 08.06.2017 : created
//-----------------------------------------------------------------------------

`ifndef _dbg
 `define _dbg

`include "kpu_conf.v"

 `define DEBUG_CONF_DEFAULT 8'b0
 `define DEBUG_IR_DEFAULT  (`MOV_OP << 26) // That's a NOP
 `define DEBUG_DATA_SEND_DEFAULT `N'b0
 `define DEBUG_DATA_RECV_DEFAULT `N'b0

module dgb(
	   input wire	       clk_i,
	   // JTAG pads
	   input wire	       tms_pad_i,
	   input wire	       tck_pad_i,
	   input wire	       trstn_pad_i,
	   input wire	       tdi_pad_i,
	   output wire	       tdo_pad_o,
	   output wire	       tdo_padoe_o,
	   // KPU pads
	   output reg	       kpu_clk_o,
	   output reg	       kpu_wait_o,
	   input wire [3:0]    kpu_d_sel_i,
	   input wire [`N-1:0] kpu_d_data_i,
	   output reg [`N-1:0] kpu_i_data_o,
	   output reg [`N-1:0] kpu_d_data_o,
	   // MEM pads
	   input wire	       mem_kpu_wait_i,
	   output reg [3:0]    mem_d_sel_o,
	   input wire [`N-1:0] mem_i_data_i,
	   input wire [`N-1:0] mem_d_data_i,
	   output reg [`N-1:0] mem_d_data_o,
	   output reg          debug_o
	   );

	////////////////////////////////////////////////////////////////////////////////////////////
	// Debug configuration register								  //
	//											  //
	// bit0	KPU clk is substituted by jtag clk when cycling in run_test/idle state		  //
	// bit1	Provide to KPU the content of debug_ir as instruction fetched			  //
	// bit2	Provide to KPU the content of debug_data_send as data fetched			  //
	// bit3	Fetch data from KPU into debug_data_recv					  //
	// bit4 When asserted let the cpu increment PC each clock                                 //
	////////////////////////////////////////////////////////////////////////////////////////////
	reg [7:0]	       debug_conf 	    = `DEBUG_CONF_DEFAULT;

	reg [`N-1:0]	       debug_ir 	    = `DEBUG_IR_DEFAULT;
	reg [`N-1:0]	       debug_data_send 	    = `DEBUG_DATA_SEND_DEFAULT;
	reg [`N-1:0]	       debug_data_recv 	    = `DEBUG_DATA_RECV_DEFAULT;
	reg [`N-1:0] 	       debug_data_recv_buf  = `DEBUG_DATA_RECV_DEFAULT;
	wire		       debug_conf_select;
	wire		       debug_ir_select;
	wire		       debug_data_send_select;
	wire		       debug_data_recv_select;
	wire		       debug_conf_tdo;
	wire		       debug_ir_tdo;
	wire		       debug_data_send_tdo;
	wire		       debug_data_recv_tdo;
	wire		       test_logic_reset;
	wire		       run_test_idle;
	wire		       capture_dr;
	wire		       shift_dr;

	tap tap_i (// JTAG
		   .tms_pad_i(tms_pad_i),
		   .tck_pad_i(tck_pad_i),
		   .trstn_pad_i(trstn_pad_i),
		   .tdi_pad_i(tdi_pad_i),
		   .tdo_pad_o(tdo_pad_o),
		   .tdo_padoe_o(tdo_padoe_o),
		   // States
		   .test_logic_reset_o(test_logic_reset),
		   .run_test_idle_o(run_test_idle),
		   .debug_conf_select_o(debug_conf_select),
		   .debug_conf_tdo_i(debug_conf_tdo),
		   .debug_ir_select_o(debug_ir_select),
		   .debug_ir_tdo_i(debug_ir_tdo),
		   .debug_data_send_select_o(debug_data_send_select),
		   .debug_data_send_tdo_i(debug_data_send_tdo),
		   .debug_data_recv_select_o(debug_data_recv_select),
		   .debug_data_recv_tdo_i(debug_data_recv_tdo),
		   .bs_chain_tdo_i(1'b0),
		   .capture_dr_o(capture_dr),
		   .shift_dr_o(shift_dr));

	/////////////////////////////////////////////////
	// Debug configuration register logic	       //
	/////////////////////////////////////////////////
	always @ (posedge tck_pad_i or negedge trstn_pad_i) begin
		if(trstn_pad_i == 0)
			debug_conf <= #1 `DEBUG_CONF_DEFAULT;   // DEBUG REG after reset
		else if (test_logic_reset)
			debug_conf <= #1 `DEBUG_CONF_DEFAULT;   // DEBUG REG after reset
		     else if(debug_conf_select & capture_dr)
			     debug_conf <= #1 `DEBUG_CONF_DEFAULT;
			  else if(debug_conf_select & shift_dr)
				  debug_conf <= #1 {tdi_pad_i, debug_conf[7:1]};
	end

	assign debug_conf_tdo = debug_conf[0];

	always @(debug_conf)
		if (`KPU_SIM_TRACE >= 3)
			$display("KPU-DBG: DEBUG CONF REG <= %b", debug_conf);
	////////////////////////////////////////////////////////
	// End of debug configuration register logic	      //
	////////////////////////////////////////////////////////

	/////////////////////////////////
	// Debug ir logic	       //
	/////////////////////////////////
	always @ (posedge tck_pad_i or negedge trstn_pad_i) begin
		if(trstn_pad_i == 0)
			debug_ir <= #1 `DEBUG_IR_DEFAULT;	 // DEBUG IR after reset
		else if (test_logic_reset)
			debug_ir <= #1 `DEBUG_IR_DEFAULT;	 // DEBUG IR OAafter reset
		     else if(debug_ir_select & capture_dr)
			     debug_ir <= #1 `DEBUG_IR_DEFAULT;
			  else if(debug_ir_select & shift_dr)
				  debug_ir <= #1 {tdi_pad_i, debug_ir[`N-1:1]};
	end

	assign debug_ir_tdo = debug_ir[0];

	always @(debug_ir)
		if (`KPU_SIM_TRACE >= 3)
			$display("KPU-DBG: DEBUG IR <= %b", debug_ir);
	////////////////////////////////////////
	// End of debug ir logic	      //
	////////////////////////////////////////

	//////////////////////////////////
	// Debug data_send logic	//
	//////////////////////////////////
	always @ (posedge tck_pad_i or negedge trstn_pad_i) begin
		if(trstn_pad_i == 0)
			debug_data_send <= #1 `DEBUG_DATA_SEND_DEFAULT;   // DEBUG DATA SEND after reset
		else if (test_logic_reset)
			debug_data_send <= #1 `DEBUG_DATA_SEND_DEFAULT;   // DEBUG DATA SEND after reset
		     else if(debug_data_send_select & capture_dr)
			     debug_data_send <= #1 `DEBUG_DATA_SEND_DEFAULT;
			  else if(debug_data_send_select & shift_dr)
				  debug_data_send <= #1 {tdi_pad_i, debug_data_send[`N-1:1]};
	end

	assign debug_data_send_tdo = debug_data_send[0];

	always @(debug_data_send)
		if (`KPU_SIM_TRACE >= 3)
			$display("KPU-DBG: DEBUG DATA SEND <= %b", debug_data_send);
	////////////////////////////////////////
	// End of debug data send logic	      //
	////////////////////////////////////////

	//////////////////////////////////
	// Debug data_recv logic	//
	//////////////////////////////////
	always @ (posedge tck_pad_i or negedge trstn_pad_i) begin
		if(trstn_pad_i == 0)
			debug_data_recv <= #1 `DEBUG_DATA_RECV_DEFAULT;   // DEBUG DATA RECV after reset
		else if (test_logic_reset)
			debug_data_recv <= #1 `DEBUG_DATA_RECV_DEFAULT;   // DEBUG DATA RECV after reset
			  else if(debug_data_recv_select & shift_dr)
				  debug_data_recv <= #1 {tdi_pad_i, debug_data_recv[`N-1:1]};
			       else
				       debug_data_recv <= #1 debug_data_recv_buf;
	end

	assign debug_data_recv_tdo = debug_data_recv[0];

	always @(debug_data_recv)
		if (`KPU_SIM_TRACE >= 3)
			$display("KPU-DBG: DEBUG DATA RECV <= %b", debug_data_recv);
	////////////////////////////////////////
	// End of debug data recv logic	      //
	////////////////////////////////////////

	always @(*) begin
		if (run_test_idle) begin
			if (debug_conf[0]) begin
				debug_o    = ~debug_conf[4];
				kpu_clk_o  = ~tck_pad_i;
			end
			else begin
				debug_o = 1'b0;
				// We run at sys clk
				kpu_clk_o  = clk_i;
			end
		end
		else begin
			debug_o = 1'b0;
			if (test_logic_reset)
				kpu_clk_o  = clk_i;
			else
				kpu_clk_o  = 1'b0;
		end

		if (run_test_idle && debug_conf[1])
			kpu_i_data_o  = debug_ir;
		else
			kpu_i_data_o  = mem_i_data_i;

		if (debug_conf[2]) begin
			kpu_d_data_o  = debug_data_send;
			kpu_wait_o = 1'b0;
		end
		else begin
			kpu_d_data_o  = mem_d_data_i;
			kpu_wait_o    = mem_kpu_wait_i;
		end

		if (debug_conf[3]) begin
			debug_data_recv_buf  = kpu_d_data_i;
			mem_d_data_o 	     = `N'b0;
			mem_d_sel_o 	     = 4'b0;
		end
		else begin
			debug_data_recv_buf  = `DEBUG_DATA_RECV_DEFAULT;
			mem_d_data_o 	     = kpu_d_data_i;
			mem_d_sel_o 	     = kpu_d_sel_i;
		end

	end

endmodule

`endif
