`timescale 1ns/1ps
//-----------------------------------------------------------------------------
// Title	 : KPU monitoring file
// Project	 : KPU
//-----------------------------------------------------------------------------
// File		 : kpu.v
// Author	 : acorallo  <andrea_corallo@yahoo.it>
// Created	 : 28.2.2017
//-----------------------------------------------------------------------------
// Description :
// Monitoring module for KPU
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2016 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 28.2.2017: created
//-----------------------------------------------------------------------------
`ifndef _monitor
 `include "kpu_conf.v"

module monitor(
	       input wire 		  clk_i,
	       input wire 		  rst_i,
	       input wire 		  stop_i,
	       input wire 		  sr_wr_i,
	       input wire [`N-1:0] 	  sr_i,
	       input wire 		  reg_wr_i,
               input wire 		  stall_i,
	       input wire 		  branching_i,
	       input wire [`N-1:0] 	  pc_i,
	       input wire [`N-1:0] 	  pc_next_i,
	       input wire [`REG_NBIT-1:0] reg_wr_n_i,
	       input wire [`N-1:0] 	  reg_wr_data_i,
	       input wire [3:0] 	  mem_wr_sel_i,
	       input wire [`N-1:0] 	  mem_wr_addr_i,
	       input wire [`N-1:0] 	  mem_wr_data_i
	       );
	reg 				  stall1;

	always @(posedge clk_i) begin
		if (`KPU_SIM_TRACE >= 1)
			if (reg_wr_i && !stop_i)
				$display("KPU: clk %0d: pc = 0x%x: Reg %0d <= 0x%x",
					 ($time / 10) + 1, pc_i, reg_wr_n_i,
					 reg_wr_data_i);
		if (`KPU_SIM_TRACE >= 4) begin
			if (rst_i)
				$display("KPU: clk %0d: pc = 0x%x: Reset",
					 $time / 10, pc_i);
			if (stop_i)
				$display("KPU: clk %0d: pc = 0x%x: Stop",
					 $time / 10, pc_i);
			if (sr_wr_i && !stop_i)
				$display("KPU: clk %0d: pc = 0x%x: SR <= %b",
					 ($time / 10) + 1, pc_i, sr_i);
			if (stall_i && !stop_i)
				$display("KPU: clk %0d: pc = 0x%x: Injecting bubble",
					 $time / 10, pc_i);
			if (branching_i && !stop_i)
				$display("KPU: clk %0d: pc = 0x%x: Branching PC <= 0x%x",
					 $time / 10, pc_i, pc_next_i);
			if (mem_wr_sel_i != 4'h0 && !stop_i)
				$display("KPU: clk %0d: pc = 0x%x: mem_sel = %b *addr 0x%x <= 0x%x",
					 ($time / 10) + 1, pc_i, mem_wr_sel_i,
					 mem_wr_addr_i << 2, mem_wr_data_i);
		end
	end // always @ (posedge clk_i)

endmodule

`endif
