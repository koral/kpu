`timescale 1ns/1ps
//-----------------------------------------------------------------------------
// Title	 : KPU pipeline control logic
// Project	 : KPU
//-----------------------------------------------------------------------------
// File		 : ctrl.v
// Author	 : acorallo  <andrea_corallo@yahoo.it>
// Created	 : 17.12.2016
//-----------------------------------------------------------------------------
// Description :
// Execution stage implementation file for KPU
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2016 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 22.12.2016 : created
// 3.4.2017 : renamed ex.v -> ctrl.v
//-----------------------------------------------------------------------------

`ifndef _ctrl
 `define _ctrl
 `include "kpu_conf.v"

module pipe_fw_ctrl(
		    input wire		wr_reg_3_i, wr_reg_4_i,
		    input wire [`N-1:0] rs1_2_i, rs2_2_i,
		    input wire [`N-1:0] data_out_3_i,
		    input wire [`N-1:0] data_4_i,
		    input wire [4:0]	rs1_n_2_i, rs2_n_2_i,
		    input wire [4:0]	reg_wr_n_3_i,
		    input wire [4:0]	reg_wr_n_4_i,
		    output wire [`N-1:0] rs1_2_o, rs2_2_o
		    );

	wire				mem1_fw, wb1_fw;

	//////////////////
	// RS 1 forward //
	//////////////////

	// Back forward from MEM stage
	assign mem1_fw = reg_wr_n_3_i == rs1_n_2_i && wr_reg_3_i == 1'b1;
	// Back forward from WB stage
	assign wb1_fw = reg_wr_n_4_i == rs1_n_2_i && wr_reg_4_i == 1'b1;
	assign rs1_2_o = mem1_fw ? data_out_3_i : (wb1_fw ? data_4_i : rs1_2_i);

	//////////////////
	// RS 2 forward //
	//////////////////

	wire				mem2_fw, wb2_fw;

	// Back forward from MEM stage
	assign mem2_fw = reg_wr_n_3_i == rs2_n_2_i && wr_reg_3_i == 1'b1;
	// Back forward from WB stage
	assign wb2_fw = reg_wr_n_4_i == rs2_n_2_i && wr_reg_4_i == 1'b1;
	assign rs2_2_o = mem2_fw ? data_out_3_i : (wb2_fw ? data_4_i : rs2_2_i);

endmodule

module pipe_stall_ctrl(
		       input wire [5:0] op_1_i,
		       input wire	alu_rrr_op_1_i,
		       input wire	alu_rri_op_1_i,
		       input wire	load_op_2_i,
		       input wire [4:0] rd_n_1_i,
		       input wire [4:0] rs1_n_1_i,
		       input wire [4:0] rs2_n_1_i,
		       input wire [4:0] rd_n_2_i,
		       output wire	stall_o);

	wire				stall_t_1, stall_t_2, staw;
	reg				ld, st;

	// Type 1 stall management
	assign stall_t_1 = load_op_2_i &
			   ((alu_rrr_op_1_i && // RRR ALU inst
			     (rd_n_2_i == rs1_n_1_i || rd_n_2_i == rs2_n_1_i)) |
			    (alu_rri_op_1_i && // RRI ALU inst
			     rd_n_2_i == rs1_n_1_i)) ?
			   1'b1 : 1'b0;

	always @(*)
	  case (op_1_i)
		  // LD MOV
		  `LDW_OP, `LDH_OP, `LDHU_OP, `LDB_OP, `LDBU_OP, `MOV_OP:
		    ld = 1'b1;
		  default:
		    ld = 1'b0;
	  endcase // case (op_1_i)

	always @(*)
	  case (op_1_i)
		  // ST
		  `STW_OP, `STH_OP, `STHU_OP, `STB_OP, `STBU_OP:
		    st = 1'b1;
		  default:
		    st = 1'b0;
	  endcase // case (op_1_i)

	assign staw = op_1_i == `STAW_OP;

	assign stall_t_2 = load_op_2_i & ((ld & (rd_n_2_i == rs1_n_1_i)) |
					  (st & (rd_n_2_i == rd_n_1_i |
						 rd_n_2_i == rs1_n_1_i)) |
					  (staw & (rd_n_2_i == rd_n_1_i))) ?
			   1'b1 : 1'b0;

	assign	stall_o	 = stall_t_1 | stall_t_2;

endmodule

`endif //  `ifndef _ctrl
