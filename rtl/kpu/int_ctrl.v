`timescale 1ns/1ps
//-----------------------------------------------------------------------------
// Title	 : KPU interrupt controller
// Project	 : KPU
//-----------------------------------------------------------------------------
// File		 : kpu.v
// Author	 : acorallo  <andrea_corallo@yahoo.it>
// Created	 : 12.3.2017
//-----------------------------------------------------------------------------
// Description :
// Interrupt controller for KPU
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2016 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 12.3.2017 : created
//-----------------------------------------------------------------------------

`ifndef _int_ctrl
 `define _int_ctrl
 `include "kpu_conf.v"

module int_ctrl(
		input wire		   clk_i,
		input wire		   rst_i,
		input wire [`IO_INT_N-1:0] io_int_i, // we support 16 interrupt lines
		output reg		   io_int_o, // interrupt line out
		output reg [`N-1:0]	   int_num_o // interrupt number raised
		);

	reg [`N-1:0]			   int_num;
	wire [`IO_INT_N-1:0]		   io_int;

	nr_int_reg nr_int_reg_i (.clk_i(clk_i),
				 .hold_i(1'b0),
				 .rst_i(rst_i),
				 .in(io_int_i),
				 .out(io_int));

	always @(*) begin
		casez (io_int)
			16'b???????????????1:
			  int_num = `N'h0;
			16'b??????????????10:
			  int_num = `N'h1;
			16'b?????????????100:
			  int_num = `N'h2;
			16'b????????????1000:
			  int_num = `N'h3;
			16'b???????????10000:
			  int_num = `N'h4;
			16'b??????????100000:
			  int_num = `N'h5;
			16'b????????10000000:
			  int_num = `N'h6;
			16'b???????100000000:
			  int_num = `N'h7;
			16'b??????1000000000:
			  int_num = `N'h8;
			16'b?????10000000000:
			  int_num = `N'h9;
			16'b????100000000000:
			  int_num = `N'ha;
			16'b???1000000000000:
			  int_num = `N'hb;
			16'b??10000000000000:
			  int_num = `N'hc;
			16'b?100000000000000:
			  int_num = `N'hd;
			16'b1000000000000000:
			  int_num = `N'he;
			default:
			  int_num = `N'hxxxxxxxx;
		endcase // casez (io_int_i)
	end // always @ (*)

	always @(posedge clk_i) begin
		if (rst_i) begin
			io_int_o  <= 1'b0;
			int_num_o <= `N'h0;
		end
		else if (~io_int_o) begin
			io_int_o  <= io_int;
			int_num_o <= int_num;
		end
	end

endmodule

`endif
