`timescale 1ns/1ps
//-----------------------------------------------------------------------------
// Title	 : KPU registers implementation file
// Project	 : KPU
//-----------------------------------------------------------------------------
// File		 : reg.v
// Author	 : acorallo  <andrea_corallo@yahoo.it>
// Created	 : 17.12.2016
//-----------------------------------------------------------------------------
// Description :
// Registers implementation for KPU
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2016 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 17.12.2016 : created
//-----------------------------------------------------------------------------

`ifndef _regr
 `define _regr
 `include "kpu_conf.v"

///////////////////////////////////////////
// General purpose syncronous flip flop	 //
///////////////////////////////////////////
module nr_ff(
	     input	clk_i,
	     input	hold_i,
	     input	rst_i,
	     input wire in,
	     output reg out);

	initial begin
		out <= 1'h0;
	end

	always @(posedge clk_i) begin
		if (rst_i)
			out <= #1 1'b0;
		else if (hold_i)
			out <= #1 out;
		     else
			     out <= #1 in;
	end
endmodule

///////////////////////////////
// Syncronous reg_n register //
///////////////////////////////
module nr_reg_n(
		input		 clk_i,
		input		 hold_i,
		input		 rst_i,
		input wire [4:0] in,
		output reg [4:0] out);

	initial begin
		out <= 5'h0;
	end

	always @(posedge clk_i) begin
		if (rst_i)
			out <= #1 5'h0;
		else if (hold_i)
			out <= #1 out;
		     else
			     out <= #1 in;
	end
endmodule


/////////////////////////////////
// Syncronous op_code register //
/////////////////////////////////
module nr_op_reg(
		 input wire	  clk_i,
		 input		  hold_i,
		 input		  rst_i,
		 input wire [5:0] in,
		 output reg [5:0] out);

	initial begin
		out <= 6'h0;
	end

	always @(posedge clk_i) begin
		if (rst_i)
			out <= #1 `NOP_OP;
		else if (hold_i)
			out <= #1 out;
		     else
			     out <= #1 in;
	end
endmodule

////////////////////////////////////////////////
// General purpose sync IO_INT_N bit register //
////////////////////////////////////////////////
module nr_int_reg(
		  input wire		     clk_i,
		  input			     hold_i,
		  input			     rst_i,
		  input wire [`IO_INT_N-1:0] in,
		  output reg [`IO_INT_N-1:0] out);

	initial begin
		out <= `IO_INT_N'h0;
	end

	always @(posedge clk_i) begin
		if (rst_i)
			out <= #1 `IO_INT_N'h0;
		else if (hold_i)
			out <= #1 out;
		     else
			     out <= #1 in;
	end
endmodule


/////////////////////////////////////////
// General purpose sync N bit register //
/////////////////////////////////////////
module nr_reg(
	      input wire	  clk_i,
	      input		  hold_i,
	      input		  rst_i,
	      input wire [`N-1:0] in,
	      output reg [`N-1:0] out);

	initial begin
		out <= `N'h0;
	end

	always @(posedge clk_i) begin
		if (rst_i)
			out <= #1 `N'h0;
		else if (hold_i)
			out <= #1 out;
		     else
			     out <= #1 in;
	end
endmodule


////////////////////
// IF-ID register //
////////////////////
module if_id(
	     input wire		 clk_i,
	     input		 hold_i,
	     input		 rst_i,
	     input		 int_enable_i,
	     input		 io_int_i,
	     input		 inv_op_int_i,
	     input		 st_ovr_int_i,
	     input wire [`N-1:0] ir_i,
	     output reg [`N-1:0] ir_o,
	     input		 debug_i,
	     output		 debug_o);

	initial begin
		ir_o <= (`NOP_OP << 26);
	end

	always @(posedge clk_i) begin
		if (rst_i)
			ir_o <= #1 (`NOP_OP << 26);
		else if (io_int_i && int_enable_i)
			ir_o <= #1 (`INT_OP << 26) | (`IO_INT_VADDR >> 2);
		else if (inv_op_int_i)
			ir_o <= #1 (`INT_OP << 26) | (`INVALID_OP_INT_VADDR >> 2);
		else if (st_ovr_int_i)
			ir_o <= #1 (`INT_OP << 26) | (`STORE_INT_VADDR >> 2);
		else if (hold_i)
			ir_o <= #1 ir_o;
		     else
			     ir_o <= #1 ir_i;
	end

	nr_ff reg_wr(.clk_i(clk_i),
		     .in(debug_i),
		     .out(debug_o),
		     .hold_i(hold_i),
		     .rst_i(rst_i));


endmodule

////////////////////
// ID-EX register //
////////////////////
module id_ex(
	     input wire		  clk_i,
	     input		  hold_i,
	     input		  rst_i,
	     input wire		  reg_wr_i,
	     output wire	  reg_wr_o,
	     input wire		  alu_rrr_op_i,
	     output wire	  alu_rrr_op_o,
	     input wire		  alu_rri_op_i,
	     output wire	  alu_rri_op_o,
	     input wire		  mem_read_i,
	     output wire	  mem_read_o,
	     input wire		  mem_wr_i,
	     output wire	  mem_wr_o,
	     input wire [5:0]	  op_i,
	     output wire [5:0]	  op_o,
	     input wire [4:0]	  rd_n_i,
	     output wire [4:0]	  rd_n_o,
	     input wire [4:0]	  rs1_n_i,
	     output wire [4:0]	  rs1_n_o,
	     input wire [4:0]	  rs2_n_i,
	     output wire [4:0]	  rs2_n_o,
	     input wire [`N-1:0]  rs1_i,
	     output wire [`N-1:0] rs1_o,
	     input wire [`N-1:0]  rs2_i,
	     output wire [`N-1:0] rs2_o,
	     input wire [`N-1:0]  imm_1_i,
	     output wire [`N-1:0] imm_2_o);

	nr_ff reg_wr(.clk_i(clk_i),
		     .in(reg_wr_i),
		     .out(reg_wr_o),
		     .hold_i(hold_i),
		     .rst_i(rst_i));

	nr_ff reg_rrr(.clk_i(clk_i),
		      .in(alu_rrr_op_i),
		      .out(alu_rrr_op_o),
		      .hold_i(hold_i),
		      .rst_i(rst_i));

	nr_ff reg_rri(.clk_i(clk_i),
		      .in(alu_rri_op_i),
		      .out(alu_rri_op_o),
		      .hold_i(hold_i),
		      .rst_i(rst_i));

	nr_ff mem_read(.clk_i(clk_i),
		      .in(mem_read_i),
		      .out(mem_read_o),
		      .hold_i(hold_i),
		      .rst_i(rst_i));

	nr_ff mem_wr(.clk_i(clk_i),
		     .in(mem_wr_i),
		     .out(mem_wr_o),
		     .hold_i(hold_i),
		     .rst_i(rst_i));

	nr_op_reg op_reg(.clk_i(clk_i),
			 .in(op_i),
			 .out(op_o),
			 .hold_i(hold_i),
			 .rst_i(rst_i));

	nr_reg_n reg_a_n(.clk_i(clk_i),
			 .in(rd_n_i),
			 .out(rd_n_o),
			 .hold_i(hold_i),
			 .rst_i(rst_i));

	nr_reg_n reg_b_n(.clk_i(clk_i),
			 .in(rs1_n_i),
			 .out(rs1_n_o),
			 .hold_i(hold_i),
			 .rst_i(rst_i));

	nr_reg_n reg_c_n(.clk_i(clk_i),
			 .in(rs2_n_i),
			 .out(rs2_n_o),
			 .hold_i(hold_i),
			 .rst_i(rst_i));

	nr_reg rs1_reg(.clk_i(clk_i),
		      .in(rs1_i),
		      .out(rs1_o),
		      .hold_i(hold_i),
		      .rst_i(rst_i));

	nr_reg rs2_reg(.clk_i(clk_i),
		      .in(rs2_i),
		      .out(rs2_o),
		      .hold_i(hold_i),
		      .rst_i(rst_i));

	nr_reg imm_reg(.clk_i(clk_i),
		       .in(imm_1_i),
		       .out(imm_2_o),
		       .hold_i(hold_i),
		       .rst_i(rst_i));
endmodule

/////////////////////
// EX-MEM register //
/////////////////////
module ex_mem(
	      input wire	   clk_i,
	      input		   hold_i,
	      input		   rst_i,
	      input wire	   reg_wr_i,
	      output wire	   reg_wr_o,
	      input wire	   mem_wr_i,
	      output wire	   mem_wr_o,
	      input wire [5:0]	   op_i,
	      output wire [5:0]	   op_o,
	      input wire [4:0]	   reg_wr_n_i,
	      output wire [4:0]	   reg_wr_n_o,
	      input wire [`N-1:0]  rs1_i,
	      output wire [`N-1:0] rs1_o,
	      input wire [`N-1:0]  alu_res_i,
	      output wire [`N-1:0] alu_res_o,
	      input wire [`N-1:0]  imm_i,
	      output wire [`N-1:0] imm_o);

	nr_ff reg_wr(.clk_i(clk_i),
		     .in(reg_wr_i),
		     .out(reg_wr_o),
		     .hold_i(hold_i),
		     .rst_i(rst_i));

	nr_ff mem_wr(.clk_i(clk_i),
		     .in(mem_wr_i),
		     .out(mem_wr_o),
		     .hold_i(hold_i),
		     .rst_i(rst_i));

	nr_op_reg op_reg(.clk_i(clk_i),
			 .in(op_i),
			 .out(op_o),
			 .hold_i(hold_i),
			 .rst_i(rst_i));

	nr_reg_n reg_a_n(.clk_i(clk_i),
			 .in(reg_wr_n_i),
			 .out(reg_wr_n_o),
			 .hold_i(hold_i),
			 .rst_i(rst_i));

	nr_reg rs1_reg(.clk_i(clk_i),
		      .in(rs1_i),
		      .out(rs1_o),
		      .hold_i(hold_i),
		      .rst_i(rst_i));

	nr_reg alu_res_reg(.clk_i(clk_i),
			   .in(alu_res_i),
			   .out(alu_res_o),
			   .hold_i(hold_i),
			   .rst_i(rst_i));

	nr_reg imm_reg(.clk_i(clk_i),
		       .in(imm_i),
		       .out(imm_o),
		       .hold_i(hold_i),
		       .rst_i(rst_i));

endmodule

/////////////////////
// MEM-WB register //
/////////////////////
module mem_wb(
	      input wire	   clk_i,
	      input		   hold_i,
	      input		   rst_i,
	      input wire	   reg_wr_i,
	      output wire	   reg_wr_o,
	      input wire [4:0]	   reg_wr_n_i,
	      output wire [4:0]	   reg_wr_n_o,
	      input wire [`N-1:0]  data_i,
	      output wire [`N-1:0] data_o);

	nr_ff reg_wr(.clk_i(clk_i),
		     .in(reg_wr_i),
		     .out(reg_wr_o),
		     .hold_i(hold_i),
		     .rst_i(rst_i));

	nr_reg_n reg_a_n(.clk_i(clk_i),
			 .in(reg_wr_n_i),
			 .out(reg_wr_n_o),
			 .hold_i(hold_i),
			 .rst_i(rst_i));

	nr_reg rs1_reg(.clk_i(clk_i),
		      .in(data_i),
		      .out(data_o),
		      .hold_i(hold_i),
		      .rst_i(rst_i));
endmodule

module kpu_regs(
		input wire		   clk_i,
		input wire		   rst_i,
		input wire		   hold_i,
		input wire [`N-1:0]	   pc_i,
		output reg [`N-1:0]	   pc_o,
		input wire		   sr_alu_wr,
		input wire [3:0]	   sr_alu_i,
		input wire		   sr_int_enable_wr,
		input wire		   sr_int_enable_i,
		output reg [`N-1:0]	   sr_o,
		input wire		   reg_wr_en_i,
		input wire [`N-1:0]	   reg_wr_i,
		input wire [`REG_NBIT-1:0] reg_wr_n_i,
		input wire [`REG_NBIT-1:0] reg1_read_n_i,
		output reg [`N-1:0]	   reg1_read_o,
		input wire [`REG_NBIT-1:0] reg2_read_n_i,
		output reg [`N-1:0]	   reg2_read_o,
		input wire [`REG_NBIT-1:0] reg3_read_n_i,
		output reg [`N-1:0]	   reg3_read_o
		);

	reg [`REG_N-1:0]		   gen_reg[`N-1:0];
	reg [3:0]			   sr_alu_tmp;
	reg				   sr_int_enable_tmp;
	reg [`REG_N-1:0]		   i;

	initial
		for (i = 0; i < `REG_N; i = i + 1)
			gen_reg[i]  = `N'h0;

	always @(posedge clk_i) begin
		if (rst_i) begin
			for (i = 0; i < `PC_N; i = i + 1)
				gen_reg[i] <= #1 `N'h0;
			// At rst_i we jump into rom
			gen_reg[`PC_N]	   <= #1 `ROM_ADDR;
		end
		else if (!hold_i) begin
			gen_reg[`PC_N] <= #1 pc_i;
			if (reg_wr_en_i)
				gen_reg[reg_wr_n_i] <= #1 reg_wr_i;
			if (sr_alu_wr)
				gen_reg[`SR_N][3:0] <= #1 sr_alu_i;
			if (sr_int_enable_wr)
				gen_reg[`SR_N][4]  <= #1 sr_int_enable_i;
		end
	end

	always @(gen_reg[reg1_read_n_i] or reg1_read_n_i or reg_wr_en_i
		 or reg_wr_i or reg_wr_n_i)
		if (reg_wr_en_i && reg1_read_n_i == reg_wr_n_i)
			reg1_read_o  = reg_wr_i;
		else
			reg1_read_o  = gen_reg[reg1_read_n_i];
	always @(gen_reg[reg2_read_n_i] or reg2_read_n_i or reg_wr_en_i
		 or reg_wr_i or reg_wr_n_i)
		if (reg_wr_en_i && reg2_read_n_i == reg_wr_n_i)
			reg2_read_o  = reg_wr_i;
		else
			reg2_read_o  = gen_reg[reg2_read_n_i];
	always @(gen_reg[reg3_read_n_i] or reg3_read_n_i or reg_wr_en_i
		 or reg_wr_i or reg_wr_n_i)
		if (reg_wr_en_i && reg3_read_n_i == reg_wr_n_i)
			reg3_read_o  = reg_wr_i;
		else
			reg3_read_o  = gen_reg[reg3_read_n_i];
	always @(gen_reg[`PC_N] or reg_wr_en_i or reg_wr_i
		 or reg_wr_n_i)
		if (reg_wr_en_i && `PC_N == reg_wr_n_i)
			pc_o	= reg_wr_i;
		else
			pc_o	= gen_reg[`PC_N];

	// SR recomposition in case of write is needed to borrow a cycle
	always @(gen_reg[`SR_N] or sr_alu_i or sr_alu_wr
		 or sr_int_enable_i or sr_int_enable_wr) begin
		if (sr_alu_wr)
			sr_alu_tmp  = sr_alu_i;
		else
			sr_alu_tmp  = gen_reg[`SR_N][3:0];

		if (sr_int_enable_wr)
			sr_int_enable_tmp  = sr_int_enable_i;
		else
			sr_int_enable_tmp	= gen_reg[`SR_N][4];

		sr_o = (sr_int_enable_tmp << 4) | sr_alu_tmp;
	end // always @ (*)

endmodule


`endif //  `ifndef _regr
