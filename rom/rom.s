;;; Basic KPU bootloader
;;; Receive a firmware on UART load it into ram and jump into it
ram:

#pragma offset 0xffff8000

        .section .text

int0:	JMP	code_start	; 0x0
	NOP			; 0x4
int1:	JMP	read_uart	; 0x8
	LDAW	R4, 0xfffffffc	; 0xC We load on R4 the int number that caused the raise
int2:	NOP			; 0x10
	NOP			; 0x14
int3:	NOP			; 0x18
	NOP			; 0x20

code_start:
	MOVI	R6, 0x0
	STAW 	R6, 0xffffffec	; We reset the uart

send_msg1:
	MOVI	R8, string1	; R8 is the pointer to the data
	MOVI	R9, 31		; R9 keep the string len
load_char1:
	LDB	R4, R8, 0
	STAW	R4, 0xffffffd8

	ADDI	R8, R8, 1
	SUBI	R9, R9, 1
	CMPI	R9, 0
	BNEQ	load_char1	; load the next character
	NOP

	MOVI	R2, 1000	; waste time for 1000 loops before restarting
wait1:
	SUBI	R2, R2, 1
	CMPI	R2, 0x0
	BNEQ	wait1
	NOP

fw_load:
	MOVI	R5, ram		; R5 is the pointer to the memory
	MOVI	R6, 24		; R6 is the shift counter
	MOVI	R8, 1024		; FW size in words

reset_uart_int: ;; Enable UART interrupt into the control register
	MOVI	R1, (1 << 3)
	STAW	R1, 0xffffffec

reset_ic:
	STAW R0, 0xfffffffc	; We reset the interrupt controller
	ORI SR, SR, (0x1 << 4)	; We enable the external interrupt

wait:
	JMP wait
	NOP

read_uart:
	;; Read received byte into R2
	LDAW	R2, 0xffffffd0
	;; Echo it back
	STAW	R2, 0xffffffd8
	;; Shift it
	SHL	R2, R2, R6
	;; Accumulate it
	OR	R7, R2, R7
	CMPI	R6, 0
	BEQ	copy_to_ram
	;;  Byte is completed
	SUBI	R6, R6, 8
	JMP	reset_ic
	NOP

copy_to_ram:
	;; Copy R7 into ram and increment the pointer
	STW	R7, R5, 0
	MOVI	R6, 24
	MOVI	R7, 0
	ADDI	R5, R5, 0x4
	SUBI	R8, R8, 1
	CMPI	R8, 0
	;; In case we have loaded the whole FW we jump into it!
	BEQ	boot_up
	NOP
	JMP	reset_ic
	NOP

boot_up:

	MOVI	R8, string2	; R8 is the pointer to the data
	MOVI	R9, 12		; R9 keep the string len

load_char2:
	LDB	R4, R8, 0
	STAW	R4, 0xffffffd8

	ADDI	R8, R8, 1
	SUBI	R9, R9, 1
	CMPI	R9, 0
	BNEQ	load_char2	; load the next character
	NOP

	ORI	SR, SR, (1 << 5) ; We move the interrupt vector base addr into ram
	JMP	ram
	NOP

        .section .data
string1:
.ascii	"KPU ROM waiting for firmware..."
string2:
.ascii	"Booting up!!"
